<?php
/**
 * Template Name: Cities list
 * Description: Page template for cities list
 * 
 * @package Urban Governance
 * @since Urban Governance 1.2
 */

get_header();

?> 
				<div class="col-md-12 content-area" id="main-column">
					<main id="main" class="site-main" role="main">
						<?php 
						while (have_posts()) {
							the_post();

							get_template_part('content', 'page');

						} //endwhile;
						?> 
					</main>
					<div class='container' >
						<div id='list' class="col-md-12" data-visualization-id="citieslist">

						</div>
					</div>
				</div>
				<script>
					loadVis();
				</script>
<?php get_footer(); ?> 
