<?php
/**
 * Template Name: City reports
 * Description: Page template for city reports
 * 
 * @package Urban Governance
 * @since Urban Governance 1.2
 */

get_header();

?>
<div class='report'>
	<div class="row map-row">
		<div id='city-info' class="col-md-4">
			<div class= "details city-data"></div>	
		</div>
		<div id='map' class="col-md-8">
		</div>
	</div>
	<div class='questions'>
	</div>
</div>

<script> 
	loadCitiesData();
	jQuery(document).scroll(function() {
	  if (jQuery(this).scrollTop()) {
		jQuery('.city-name').fadeIn(400);
	  } else {
		jQuery('.city-name').fadeOut(600);
	  }
	});
	  	
</script>
					
<?php get_footer(); ?> 
