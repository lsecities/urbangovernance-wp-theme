# Urban Governance survey microsite - WordPress theme

This WordPress theme is used for the microsite showcasing the data
visualizations related to the 2015/2016 Urban Governance survey.

The visualizations are implemented with D3 and are each attached to a distinct
WordPress template.

## Support for alternative datasets

This new feature allows to configure datasets for the visualizations, besides
the initially hardcoded dataset. This is useful when testing how updated
datasets (with more responses to the questionnaire from new cities) work with
the visualization code, in order to identify any possible glitches before
'promoting' the updated dataset to production.

