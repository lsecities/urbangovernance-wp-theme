<?php
/**
 * Template Name: Exploring the data
 * Description: Page template for data explorer
 * 
 * @package Urban Governance
 * @since Urban Governance 1.1
 */

get_header();

?> 
	<script>
	  jQuery(document).ready(function(){
		jQuery("#sticker1").sticky({topSpacing:0});
	  });
	</script> 
				<div class="col-md-12 content-area" id="main-column">
					<main id="main" class="site-main" role="main">
						<?php 
						while (have_posts()) {
							the_post();

							get_template_part('content', 'page');

						} //endwhile;
						?> 
					</main>
					<div id="sticker1">
					<div class='vis-header container' >	
						<div class="row selection-row">
							<div class="col-xs-3 col-md-1 sortby"></div>
							<div class="col-xs-9 col-md-6">
								<div class="dropdown">
									<div id="mainitem-sort">
										<div class="selected-item-sort pull-left">Revenue vs Expenditure</div> 
										<div class="split pull-right"></div>				
									</div>
									<div id="subitem-sort">
										<ul></ul>
									</div>			
								</div>		
							</div>
							<div class="col-xs-3 col-md-2 colourby"></div>
							<div class= "col-xs-9 col-md-3">
								<div class="dropdown">
									<div id="mainitem-colour">
										<div class="selected-item pull-left">World Region</div> 
										<div class="split pull-right"></div>
									</div>
									<div id="subitem-colour">
										<ul></ul>
									</div>	<!-- subitem -->		
								</div>
							</div>
						</div>
						<div class="row legend-row">
							<div class="col-xs-12 col-md-5 legend-sort">
							</div>
							<div class= "col-xs-12 col-md-7 legend-color">
							</div>
						</div>
					</div>
					</div>
					<div class='container explorevis-container' >
						<div class="row">
							<div id='vis' class="col-md-12" data-visualization-id="explorevis">
							</div>
						</div>	
					</div>
				</div>
				
				<script>loadVis()</script>
				
<?php get_footer(); ?> 
