<?php
/**
 * Bootstrap Basic theme
 *
 * @package bootstrap-basic
 */

/**
 * Remove ver=X.Y.Z GET query args to make Service Worker's life easier
 */
function vc_remove_wp_ver_css_js( $src ) {
  if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) )
        $src = remove_query_arg( 'ver', $src );
  return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );

/**
 * Add Service Worker JS snippet to footer
 */
function service_worker_in_footer() {
  echo "
<script>
if (navigator.serviceWorker) {
    navigator.serviceWorker.register('/serviceworker.js', {
        scope: '/'
    });
    window.addEventListener('load', function() {
        if (navigator.serviceWorker.controller) {
            navigator.serviceWorker.controller.postMessage({'command': 'trimCaches'});
        }
    });
}
</script>";
}

add_action('wp_footer', 'service_worker_in_footer');


/**
 * Required WordPress variable.
 */
if (!isset($content_width)) {
	$content_width = 1170;
}


if (!function_exists('bootstrapBasicSetup')) {
	/**
	 * Setup theme and register support wp features.
	 */
	function bootstrapBasicSetup()
	{
		/**
		 * Make theme available for translation
		 * Translations can be filed in the /languages/ directory
		 *
		 * copy from underscores theme
		 */
		load_theme_textdomain('bootstrap-basic', get_template_directory() . '/languages');

		// add theme support post and comment automatic feed links
		add_theme_support('automatic-feed-links');

		// enable support for post thumbnail or feature image on posts and pages
		add_theme_support('post-thumbnails');
		set_post_thumbnail_size( 362, 228, true ); // default Post Thumbnail dimensions (cropped)

		// add support menu
		register_nav_menus(array(
			'primary' => __('Primary Menu', 'bootstrap-basic'),
		));

		// add post formats support
		add_theme_support('post-formats', array('aside', 'image', 'video', 'quote', 'link'));

		// add support custom background
		add_theme_support(
			'custom-background',
			apply_filters(
				'bootstrap_basic_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => ''
				)
			)
		);
	}// bootstrapBasicSetup
}
add_action('after_setup_theme', 'bootstrapBasicSetup');


if (!function_exists('bootstrapBasicWidgetsInit')) {
	/**
	 * Register widget areas
	 */
	function bootstrapBasicWidgetsInit()
	{
		register_sidebar(array(
			'name'          => __('Header right', 'bootstrap-basic'),
			'id'            => 'header-right',
			'before_widget' => '',
			'after_widget'  => '</div>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		));

		register_sidebar(array(
			'name'          => __('Navigation bar right', 'bootstrap-basic'),
			'id'            => 'navbar-right',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '',
			'after_title'   => '',
		));

		register_sidebar(array(
			'name'          => __('Sidebar left', 'bootstrap-basic'),
			'id'            => 'sidebar-left',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		));

		register_sidebar(array(
			'name'          => __('Sidebar right', 'bootstrap-basic'),
			'id'            => 'sidebar-right',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		));
		/* custom sidebar for urban governance: case study sidebar */
		register_sidebar(array(
			'name'          => __('Sidebar right case study', 'bootstrap-basic'),
			'id'            => 'sidebar-right-study-case',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		));

		/* custom sidebar for urban governance: top text area for posts home */
		register_sidebar(array(
			'name'          => __('Posts home - header', 'bootstrap-basic'),
			'id'            => 'sidebar-header-home',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		));

		register_sidebar(array(
			'name'          => __('Footer left', 'bootstrap-basic'),
			'id'            => 'footer-left',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		));

		register_sidebar(array(
			'name'          => __('Footer right', 'bootstrap-basic'),
			'id'            => 'footer-right',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		));
	}// bootstrapBasicWidgetsInit
}
add_action('widgets_init', 'bootstrapBasicWidgetsInit');


if (!function_exists('bootstrapBasicEnqueueScripts')) {
	/**
	 * Enqueue scripts & styles
	 */
	function bootstrapBasicEnqueueScripts()
	{
		wp_enqueue_style('bootstrap-style', get_template_directory_uri() . '/css/bootstrap.min.css');
		wp_enqueue_style('bootstrap-theme-style', get_template_directory_uri() . '/css/bootstrap-theme.min.css');
		wp_enqueue_style('fontawesome-style', get_template_directory_uri() . '/css/font-awesome.min.css');
		wp_enqueue_style('main-style', get_template_directory_uri() . '/css/main.css');

    wp_enqueue_script('jquery'); /*for bootstrap*/

		wp_enqueue_script('modernizr-script', get_template_directory_uri() . '/js/vendor/modernizr.min.js');
		wp_enqueue_script('respond-script', get_template_directory_uri() . '/js/vendor/respond.min.js');
		wp_enqueue_script('html5-shiv-script', get_template_directory_uri() . '/js/vendor/html5shiv.js');
		wp_enqueue_script('bootstrap-script', get_template_directory_uri() . '/js/vendor/bootstrap.min.js', 'jquery');
		wp_enqueue_style('bootstrap-basic-style', get_stylesheet_uri());

		// main app script, browserified - in footer
		//wp_enqueue_script('urbangovernance-survey-app', get_template_directory_uri() . '/js/app.js', '', '', TRUE);

		if (is_front_page()){
			wp_enqueue_style('jquery-ui', get_template_directory_uri() . '/css/jquery-ui.css');
			wp_enqueue_style('surveyvis', get_template_directory_uri() . '/css/surveyvis.css');
			wp_enqueue_style('token-input', get_template_directory_uri() . '/css/token-input.css');
			wp_enqueue_script('d3', get_template_directory_uri() . '/js/d3.min.js');
			wp_enqueue_script('language_data', get_template_directory_uri() . '/js/language_data.js');
			wp_enqueue_script('jquery-sticky', get_template_directory_uri() . '/js/jquery.sticky.js');
			wp_enqueue_script('jquery-tokeninput', get_template_directory_uri() . '/js/jquery.tokeninput.js');
			wp_enqueue_script('jqueue', get_template_directory_uri() . '/js/queue.js');
			wp_enqueue_script('scroller', get_template_directory_uri() . '/js/scroller.js');
			wp_enqueue_script('surveyvis', get_template_directory_uri() . '/js/surveyvis.js');
		}
		if (is_page_template('pagetemplate-data-explorer.php')){
			wp_enqueue_style('explorevis', get_template_directory_uri() . '/css/explorevis.css');
			wp_enqueue_script('d3', get_template_directory_uri() . '/js/d3.min.js');
			wp_enqueue_script('jqueue', get_template_directory_uri() . '/js/queue.js');
			wp_enqueue_script('jquery-sticky', get_template_directory_uri() . '/js/jquery.sticky.js');
			wp_enqueue_script('explorevis', get_template_directory_uri() . '/js/explorevis.js');
		}
		if (is_page_template('pagetemplate-city-reports.php')){
			wp_enqueue_style('reports', get_template_directory_uri() . '/css/reports.css');
			wp_enqueue_script('d3', get_template_directory_uri() . '/js/d3.min.js');
			wp_enqueue_script('language_data', get_template_directory_uri() . '/js/language_data.js');
			wp_enqueue_script('jquery-sticky', get_template_directory_uri() . '/js/jquery.sticky.js');
			wp_enqueue_script('jqueue', get_template_directory_uri() . '/js/queue.js');
			wp_enqueue_script('reports', get_template_directory_uri() . '/js/reports.js');
		}
		if (is_page_template('pagetemplate-cities-list.php')){
			wp_enqueue_style('citieslist', get_template_directory_uri() . '/css/citieslist.css');
			wp_enqueue_script('d3', get_template_directory_uri() . '/js/d3.min.js');
			wp_enqueue_script('jquery-tokeninput', get_template_directory_uri() . '/js/jquery.tokeninput.js');
			wp_enqueue_script('jqueue', get_template_directory_uri() . '/js/queue.js');
			wp_enqueue_script('citieslist', get_template_directory_uri() . '/js/citieslist.js');
		}

	}// bootstrapBasicEnqueueScripts
}
add_action('wp_enqueue_scripts', 'bootstrapBasicEnqueueScripts');


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';


/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';


/**
 * Custom dropdown menu and navbar in walker class
 */
require get_template_directory() . '/inc/BootstrapBasicMyWalkerNavMenu.php';


/**
 * Template functions
 */
require get_template_directory() . '/inc/template-functions.php';


/**
 * --------------------------------------------------------------
 * Theme widget & widget hooks
 * --------------------------------------------------------------
 */
require get_template_directory() . '/inc/widgets/BootstrapBasicSearchWidget.php';
require get_template_directory() . '/inc/template-widgets-hook.php';


/**
 * excerpt length
 */

 function custom_excerpt_length( $length ) {
	return 15;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


/**
 * Polylang string translation
 */
require get_template_directory() . '/inc/translation.php';

/**
 * Allow upload of CSV and JSON files
 * This is needed in order to manage alternative datasets via the site's
 * Data Packages pods.
 */
function allow_dataset_uploads($existing_mimes=[]) {
	$existing_mimes['csv'] = 'text/csv';
	$existing_mimes['json'] = 'application/json';

	// return amended array
 	return $existing_mimes;
}

add_filter('upload_mimes', 'allow_dataset_uploads');

/**
 * Load theme classes
 */
if(!class_exists('\\UrbanGovernanceWPTheme\\DataPackage')) {
	require_once(get_stylesheet_directory() . '/lib/urbangovernance/data-package/data-package.php');
}
