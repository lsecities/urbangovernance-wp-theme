<?php
/**
 * The theme header
 * 
 * @package bootstrap-basic
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>     <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>     <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php wp_title('|', true, 'right'); ?></title>
		<meta name="viewport" content="width=device-width">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<!--wordpress head-->
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<!--[if lt IE 8]>
			<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
		<![endif]-->
<div id="wrap">
<header role="banner">
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
		<?php do_action('before'); ?> 
			<div class="row main-title">
				<div class="row">
					<div class="col-xs-12 col-md-6"><h1>
						<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
						<div class="site-description">
							<?php bloginfo('description'); ?> 
						</div>
					</div>
					<div class="col-xs-12 col-md-6 logos text-right">
						<a href="https://lsecities.net/"><img src="<?php bloginfo('template_directory'); ?>/img/logo_lsecities.png" alt="LSE Cities"></a>
						<a href="http://unhabitat.org/"><img src="<?php bloginfo('template_directory'); ?>/img/logo_unhabitat.png" alt="UN Habitat"></a>
						<a href="http://www.uclg.org/"><img src="<?php bloginfo('template_directory'); ?>/img/logo_uclg_en.png" alt="UCLG"></a>
						<a href="http://www.macarthur.org/" class="last"><img src="<?php bloginfo('template_directory'); ?>/img/logo_macarthur.png" alt="MacArthur Foundation"></a>
					</div>	
				</div>
			</div>
			<div class="row nav-header">
				<div class="main-navigation" role="navigation">
					<div class="col-xs-9 col-sm-10">
					<!-- .btn-navbar is used as the toggle for collapsed navbar content >

						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<div class="navbar-collapse collapse"-->
						  <?php
						  if ( has_nav_menu( 'primary' ) ) {
							 wp_nav_menu(
								array(
								   'theme_location' => 'primary',
								   'menu_class'    => 'menu-primary-container'
								)
							 );
						  }
						  else {
							 wp_page_menu();
						  }
					 	 ?>
					 	<!--/div-->
					</div>
					<div class="col-xs-3 col-sm-2">
						<?php if (is_active_sidebar('navbar-right') && !is_page_template( 'pagetemplate-city-reports.php' )) { 
						?> 
						<div class="widget-area">
							<?php dynamic_sidebar('navbar-right'); ?> 
						</div>
						<?php } // endif; ?> 
					</div>
				</div>  
			</div>
			<div class="row surveyvis-header">
				<div class="col-sm-12 col-md-4">
					<div class="theme-title"></div>
				</div>
				<div class="col-xs-7 col-md-5">
					<input class="form-control" type="text" placeholder="Highlight cities" id="city-search" ></input>
				</div>
				<div class= "col-xs-5 col-md-3">
					<div class="colourby pull-left">
						Colour by:
					</div>
					<div class="dropdown pull-right">
						<div id="mainitem">
							<div class="selected-item pull-left">World Region</div> 
							<div class="split pull-right"></div>
						</div>
						<div id="subitem">
							<ul></ul>
						</div>			
					</div>
				</div>
			</div>
		</div><!-- container fluid -->
		<?php if (is_page_template( 'pagetemplate-city-reports.php' )) { ?> 
		<div class="container">
			<div class="row cityreport-header">
				<div class="city-name col-md-12"></div>
			</div>
		</div> 
		<?php } // endif; ?> 
	</nav>
</header>
<div class="container-fluid nav-breadcrumb">
	<div class="row">	
		<div class="col-md-12">
			<div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<?php if(function_exists('bcn_display'))
				{
					bcn_display();
				}?>
			</div>
		</div>
	</div>
</div>
<?php 
if( is_front_page() ) { ?>
	<div class= "container-fluid page-container">
<?php
}
else {  ?>
<div class= "container page-container">
<?php 
} 
if( is_home() ) { ?>
	<div id="content" class="site-content">
<?php
}
else {  ?>
	<div id="content" class="row site-content">
<?php 
} ?>

			
			