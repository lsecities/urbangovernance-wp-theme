var displayCities = function(error, surveyData){
  // get current page language from the lang attribute of the html
  // element (this is set by the polylang WP plugin); default is en-GB
  var current_language = jQuery('html').attr('lang') || 'en-GB';

	// single object with all variables that require translations
  var language_data = {
    sortByOptions: [
      {
        id: 'exp-rev',
        name: {
          'en-GB': 'Revenue vs Expenditure',
          'es-ES': 'Ingreso vs Gasto',
          'fr-FR': 'Revenus vs. Dépenses'
        },
        value: '_exprev'
      },
      {
        id: 'inf-eco',
        name: {
          'en-GB': '% of informal economy',
          'es-ES': '% de la economía informal',
          'fr-FR': '% économie informelle'
        },
        value: '_infeco'
      },
      {
        id: 'num-pol',
        name: {
          'en-GB': 'No. of policy sectors led by the city',
          'es-ES': 'Núm. de sectores de política pública liderados por la ciudad',
          'fr-FR': 'No de politiques sectorielles dirigées par la ville'
        },
        value: 'Number of policy sectors led by the city'
      },
      {
        id: 'loc-pol',
        name: {
          'en-GB': 'No. of participatory mechanisms available to citizens',
          'es-ES': 'Núm. de mecanismos de participación a disposición de los ciudadanos',
          'fr-FR': 'No de mécanismes de participation disponibles pour les citoyens'
        },
        value: 'Citizens participation'
      },
      {
        id: 'voter',
        name: {
          'en-GB': 'Voters Turnout (latest election)',
          'es-ES': 'Participación electoral (última elección)',
          'fr-FR': 'Taux de participation électorale (dernière élection)'
        },
        value: 'q10a'
      }
    ],
    colourByOptions: [
      { id: 'reg',
        name: {
          'en-GB': 'World Region',
          'es-ES': 'Región del Mundo',
          'fr-FR': 'Région du monde'
        },
        value:'UN Region Level 2'
      },
      {
        'id':'pop',
        'name': {
          'en-GB': 'Population',
          'es-ES': 'Población',
          'fr-FR': 'Population'
        },
        value:'q03a'
      },
      {
        'id':'gdp',
        'name': {
          'en-GB': 'Wealth',
          'es-ES': 'Riqueza',
          'fr-FR': 'Richesse'
        },
        value:'GDP/capita ($PPP)'
      }
    ],
    income_levels: {
      'en-GB': [ 'low income', 'middle income', 'high income' ],
      'es-ES': [ 'ingreso bajo', 'ingreso medio', 'ingreso alto' ],
      'fr-FR': [ 'bas revenus', 'revenus moyens', 'haut revenus' ]
    },
    continent_colors: {
      'Africa': {
        name: {
          'en-GB': 'Africa',
          'es-ES': 'África',
          'fr-FR': 'Afrique'
        },
        color: '#D97196',
      },
      'Latin America and the Caribbean': {
        name: {
          'en-GB': 'Latin America and the Caribbean',
          'es-ES': 'América Latina y el Caribe',
          'fr-FR': 'Amerique Latine et Caraibes'
        },
        color: '#F69A4D',
      },
      'Asia': {
        name: {
          'en-GB': 'Asia',
          'es-ES': 'Asia',
          'fr-FR': 'Asie'
        },
        color: '#8781A7',
      },
      'Europe': {
        name: {
          'en-GB': 'Europe',
          'es-ES': 'Europa',
          'fr-FR': 'Europe'
        },
        color: '#67B064',
      },
      'Northern America': {
        name: {
          'en-GB': 'Northern America',
          'es-ES': 'Norteamérica',
          'fr-FR': 'Amerique du Nord'
        },
        color: '#3393C1',
      },
      'Oceania': {
        name: {
          'en-GB': 'Oceania',
          'es-ES': 'Oceanía',
          'fr-FR': 'Oceanie'
        },
        color: '#FF0009',
      }
    },
    strings: {
      'en-GB': {
        no_answer: 'No answer ',
        population: 'Population',
        show_legend: 'Show Legend',
        hide_legend: 'Hide Legend',
        people_working_for_the_city_local_government: 'People working for the city/local government',
        revenue: 'Revenue',
        expenditure: 'Expenditure',
        highlight_cities: 'Highlight cities',
        colour_by: 'Colour by:',
        sort_by: 'Sort by:',
        cta_scroll_slowly: 'Scroll slowly to see the charts',
        cta_click_on_a_dot: 'Click on a dot for information about a city'
      },
      'es-ES': {
        no_answer: 'Sin respuesta ',
        population: 'Población',
        show_legend: 'Ver Leyenda',
        hide_legend: 'Esconder Leyenda',
        people_working_for_the_city_local_government: 'Num. de personas empleadas por el gobierno local',
        revenue: 'Ingreso',
        expenditure: 'Gasto',
        highlight_cities: 'Muestra la ciudad',
        colour_by: 'Color:',
        sort_by: 'Ordenar:',
        cta_scroll_slowly: 'Desplazar lentamente hacia abajo para ver los gráficos',
        cta_click_on_a_dot: 'Haga clic en un punto para ver la información de una ciudad'
      },
      'fr-FR': {
        no_answer: 'Pas de réponse ',
        population: 'Population',
        show_legend: 'Montrer la légende',
        hide_legend: 'Cacher la légende',
        people_working_for_the_city_local_government: 'Nombre de personnes travaillant pour le gouvernement local',
        revenue: 'Revenus',
        expenditure: 'Dépenses',
        highlight_cities: 'Montrer la ville',
        colour_by: 'Couleur:',
        sort_by: 'Trier:',
        cta_scroll_slowly: 'Faites défiler vers le bas pour voir les graphiques',
        cta_click_on_a_dot: 'Cliquer sur les points pour voir les informations sur une ville'
      },
    }
  };

  var width = jQuery('#vis').width();
	var rawHeight = 4000;
	var margin = {top:50, left:0, bottom:10, right:0};
	var height = rawHeight -margin.top-margin.bottom;
	var scale = width * 140 / 878;

    var infEconLabels = ["don't know or no answer","0-5","5-10","10-20","20-30","30-40","40-50","50-60","60-70","70-80","70-80","> 90","I don't know."];

	var activeColour = 'reg';//'UN Region Level 2';
	var activeIndicator = '_exprev';

	var cityCircles, cityInfo, cityData;

	var maxValue = {}, minValue = {}, minValueColour = {}, maxValueColour = {};

	var continentColors = language_data.continent_colors;

	var populationColors = ['#ffCCCD','#ff7f84','#ff0009','#B20006','#7f0004']

	var populationQuantile = d3.scale.quantile()
		.domain([0,500000,1000000,5000000,10000000,100000000])
		.range(populationColors)

	var wealthColors = ['#ff0009', '#ff8504', '#fac808'].reverse();

	var wealthQuantile = d3.scale.quantile()
		.domain([0,2000,20000,100000000])
		.range(wealthColors)

	var surveyDataByCity = d3.map();

	var projection = d3.geo.equirectangular()
		.scale(scale)
		.rotate([-10,0])
		.translate([width / 2, 280])
		.precision(.1);

	var path = d3.geo.path()
		.projection(projection);

	var tooltipdiv = d3.select("body")
		.append("div")
		.attr("class", "arrow_box");

	var legenddiv = d3.select(".legend-color")
		.append("div")
		.attr("class", "legenddiv")

  // set translated text for UI elements that have text set in HTML markup
	jQuery('.sortby').text(language_data.strings[current_language].sort_by);
  	jQuery('.colourby').text(language_data.strings[current_language].colour_by);

	d3.select('#mainitem-sort .selected-item-sort')
		.html(language_data.sortByOptions.filter(function(i) { return i.id === 'exp-rev'; })[0].name[current_language]);

	d3.select('#mainitem-colour .selected-item')
		.html(language_data.colourByOptions.filter(function(i) { return i.id === 'reg'; })[0].name[current_language]);

	//console.log('---',language_data.strings[current_language].colour_by)
	// continent legend
	var continentLegend = legenddiv.append('div')
		.attr('class','reg-legend legend')
		.append('svg')
		.attr('width',602)
		.attr('height',60)
		.append('g')
		.attr("transform","translate(0,50)")
		.selectAll('.legendItem')
		.data(d3.entries(continentColors))
		.enter()
		.append("g")
		.attr('class','legendItem')

	continentLegend.append('circle')
		.attr('cx', 10)
		.attr('cy', -4)
		.attr('fill',function(d){return d.value.color})
		.attr('r',5)

	continentLegend.append('text')
		.attr('x', 18)
		.style("text-anchor", 'start')
		.text(function(d){return d.value.name[current_language];})

	rearrangeLegendItem('reg-legend')

	// population legend
	var populationLegend = legenddiv.append('div')
		.attr('class','pop-legend legend')
		.append('svg')
		.attr('width',660)
		.attr('height',60)
		.append('g')
		.attr("transform","translate(0,50)")
		.selectAll('.legendItem')
		.data([' < 500,000', '500,000 - 1,000,000','1,000,000 - 5,000,000','5,000,000 - 10,000,000', ' >  10,000,000'])
		.enter()
		.append("g")
		//.attr("transform",function(d,i){ return "translate(0,"+ i*22 +")"})
		.attr('class','legendItem')

	populationLegend.append('circle')
		.attr('cx', 10)
		.attr('cy', -4)
		.attr('fill',function(d,i){return populationColors[i]})
		.attr('r',5)

	populationLegend.append('text')
		.attr('x', 18)
		.style("text-anchor", 'start')
		.text(function(d){return d})

	rearrangeLegendItem('pop-legend')

	legenddiv.select('.pop-legend').style('display','none')


	// wealth legend
	var wealthLegend = legenddiv.append('div')
		.attr('class','gdp-legend legend')
		.append('svg')
		.attr('width',305)
		.attr('height',60)
		.append('g')
		.attr("transform","translate(0,50)")
		.selectAll('.legendItem')
		.data(['low income', 'middle income', 'high income'])
		.enter()
		.append("g")
		.attr('class','legendItem')

	wealthLegend.append('circle')
		.attr('cx', 10)
		.attr('cy', -4)
		.attr('fill',function(d,i){return wealthColors[i]})
		.attr('r',5)

	wealthLegend.append('text')
		.attr('x', 18)
		.style("text-anchor", 'start')
		.text(function(d){return d})

	rearrangeLegendItem('gdp-legend')
	legenddiv.select('.gdp-legend').style('display','none')

/* sort legend */
	var legenddivsort = d3.select(".legend-sort")
		.append("div")
		.attr("class", "legenddiv")

	var sortLegend = legenddivsort.append('div')
		.attr('class','size-legend legend')
		.append('svg')
		.attr('width',250)
		.attr('height',87)
		.append('g')
		.attr("transform","translate(15,50)")
		.selectAll('.legendItem')
		.data([{'label':'min','size':5},{'label':'max','size':40}])
		.enter()
		.append("g")
		.attr('class',function(d){ return 'legendItem '+d.label})

	sortLegend.append('circle')
		.attr('cx', 10)
		.attr('cy', -4)
		.attr('fill','#CCC')
		.attr('r',10)

	sortLegend.append('text')
		//.attr('x', 18)
		.style("text-anchor", 'start')
		.text(function(d){return d.label})

	var svg = null;
	var g = null;
	var labelsGroup = null;
	var qCircles = null;

	var citySelected, citySelectedId;

	// circle size
	var sortByOptions = language_data.sortByOptions;

	d3.select('#subitem-sort ul').selectAll('li')
		.data(sortByOptions)
		.enter()
		.append('li')
		.attr('id',function(d){return d.id})
		.text(function(d){return d.name[current_language]})
		.on('click', sortBy)

	//change array depending on language
	var colourByOptions= language_data.colourByOptions;

	d3.select('#subitem-colour ul').selectAll('li')
		.data(colourByOptions)
		.enter()
		.append('li')
		.attr('id',function(d){return d.id})
		.text(function(d){ return d.name[current_language]})
		.on('click', colourBy)

	var iconWidth = 100;
	var iconHeight = 120;
	var iconMargin = 20;
	var iconsPerRow = Math.floor(width/iconWidth);
	var rawHeight = (Math.floor(surveyData.length/iconsPerRow)+1)*iconHeight + iconMargin;

	svg = d3.select("#vis").append("svg")
		.attr("width", width+ margin.left + margin.right)
		.attr("height", rawHeight)
		.append('g')
		.attr('transform',"translate("+margin.left+','+margin.top+")")
		.style("display","block")
		.style("margin","auto")

	g = svg.append("g").attr('class','circles')

	var allCityNames = [];

	surveyDataByCity = d3.map(surveyData, function(d) {return d.q01x.trim()})

	//initializing different values for the cities
	surveyData.forEach(function(d,i){
		d.q01x = d.q01x.trim();
		d.x = {};
		d.y = {};
		d.radius = {};
		d._id = i;

		if ((!isNaN(d.q17b) && !isNaN(d.q18b)) && (d.q17b > 0 && d.q18b > 0 )) d._exprev = (d.q17b/d.q18b)*100;
		else d._exprev = -1;

		if (!isNaN(d.q17b) && !isNaN(d.q03a)) d._revpercap = (d.q17b/d.q03a);
		else d._revpercap = -1;

		// cleaning up data
		d._infeco = +surveyDataByCity.get(d.q01x.trim()).q12b;
		if (d._infeco == 12) d._infeco = 0; // 12 is "I don't know"

		d['Number of policy sectors led by the city'] = (d['Number of policy sectors led by the city'] == 'n/a')? -1 : parseInt(d['Number of policy sectors led by the city']);
		d.q10a = (d.q10a)? Number(d['q10a'].replace(',', '.').replace('%','')): -1;

		allCityNames.push({'name':d.q01x,'id':i})
	})

	surveyData.sort(function(obj1, obj2) {
		var textA = obj1['UN Region Level 2'].toUpperCase();
		var textB = obj2['UN Region Level 2'].toUpperCase();
		return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
	});

	sortByOptions.forEach(function(g){
		var indicator = g['value'];
		if (g.id == 'inf-eco') minValue[indicator] = 1;
		else minValue[indicator] = d3.min(surveyData, function(d){if (!isNaN(d[indicator]) && d[indicator] >= 0) return +d[indicator]});
		maxValue[indicator] = d3.max(surveyData, function(d){if (!isNaN(d[indicator]) && d[indicator] >= 0) return +d[indicator]});
	})
	colourByOptions.forEach(function(g){
		var indicator = g['value'];
		minValueColour[indicator] = d3.min(surveyData, function(d){if (!isNaN(d[indicator]) && d[indicator] >= 0) return +d[indicator]});
		maxValueColour[indicator] = d3.max(surveyData, function(d){if (!isNaN(d[indicator]) && d[indicator] >= 0) return +d[indicator]});
	})

	surveyData.forEach(function(d,i){
		d.pos = {};
		d.rank = {};
		d.order = i;
	})

	sortByOptions.forEach(function(g){ //recalculating for all indicators
		var indicator = g.value;
		surveyData.sort(function(a, b){
			 return (b[indicator] - a[indicator]);
		})

		surveyData.forEach(function(d, i){
			var x = (i - iconsPerRow * Math.floor(i/iconsPerRow)) * iconWidth + iconMargin;
			var y = Math.floor(i/iconsPerRow) * iconHeight + iconMargin;
			d.pos[indicator] = [x,y];
		})
	})



	var scaleCircle = function(indicator,value){
		var range = [5,40];
		var domain = [minValue[indicator],maxValue[indicator]];
		var scale = d3.scale.sqrt()
		.domain(domain)
		.range(range);
		return scale(value);
	}

	sortLegendContent(sortByOptions[0])

	cityCircles = g.selectAll(".city")
		.data(surveyData)
		.enter()
		.append('g')
		.attr('class','city')
		.attr("transform", function(d){return "translate("+d.pos['_exprev'][0]+","+d.pos['_exprev'][1]+")";})
		.on("mouseover", mouseover)
		.on("mouseout", mouseout)

	cityCircles.append('circle')
		.attr('cx', iconWidth/2)
		.attr('cy', 10)
		.attr('r', function(d){ return scaleCircle('_exprev',d._exprev)})
		.attr('fill',function(d){return continentColors[d['UN Region Level 2']].color})
		.style('pointer-events','all')


	cityCircles.append('g').attr('transform','translate(50,-40)')
		.attr('class','label')
		.append("text")
		//.attr("x",iconWidth)
		.attr("y",0)
		.attr("dy",0)
		.text(function(d){return d.q01x})
		.call(wrap,90)

	cityCircles.filter(function(d){ return (d._exprev < 0)})
		.style('opacity',0)
		.style('pointer-events','none')

	/*---- single choice functions --- */

	function circleColour(d){
		if (activeColour == 'pop') return populationQuantile(d.q03a);
		else if (activeColour == 'reg') return continentColors[d['UN Region Level 2']].color
		else if (activeColour == 'gdp'){
			var gdp = d['GDP/capita ($PPP)'];
			return wealthQuantile(gdp);
		}
		else if (activeColour =='exp') return expenditureQuantile(d._exppercap);
		else if (activeColour =='rev') return revenueQuantile(d._revpercap);
	}

	function colourBy(d){
		activeColour = d.id;
		jQuery('.selected-item').text(d.name[current_language]);
		jQuery('#subitem-colour').slideUp()
		d3.selectAll('.city circle')
			.attr('fill', function(g){return circleColour(g)} )

		//legend
		legenddiv.selectAll('.legend').style('display','none');
		legenddiv.select('.'+ d.id +'-legend').style('display','block')
	}

	function sortBy(g){
		activeIndicator = g.value;
		jQuery('.selected-item-sort').text(g.name[current_language]);
		jQuery('#subitem-sort').slideUp()

		var transition = cityCircles
			.style('pointer-events','all')
			.transition()
			.duration(1000)
			.style('opacity',1)

		transition.attr("transform", function(d){return "translate("+d.pos[g.value][0]+","+d.pos[g.value][1]+")";})
		transition.filter(function(d){ return (d[g.value] <= 0)})
			.style('opacity',0)
			.style('pointer-events','none')
		transition.select('circle')
			.attr('r', function(d){return scaleCircle(g.value,d[g.value])})

		sortLegendContent(g)

	}

	function mouseover(d){
		var sortOption = jQuery.grep(sortByOptions, function(e){ return e.value == activeIndicator; });
		var colourOption = jQuery.grep(colourByOptions, function(e){ return e.id == activeColour; });
		var value = formatSizeValue(d[activeIndicator]);

		if (activeColour == 'pop') var colourValue = d[colourOption[0].value]
    else if (activeColour == 'reg' ) var colourValue = language_data.continent_colors[d[colourOption[0].value]].name[current_language]
		else if (activeColour == 'gdp') var colourValue = d[colourOption[0].value];
		else var colourValue = d[colourOption[0].value].toFixed(2);

		var textTooltip = '<div class="title">'+d.q01x+', '+d.q01 +'</div>'+sortOption[0].name[current_language]+': <strong>'+ value+'</strong></br>'+ colourOption[0].name[current_language]+': <strong>'+ colourValue+'</strong>';
		//+'</br>Expenditure: '+numberWithCommasDolarFormat(d.q18b)+'</br>Revenue: '+numberWithCommasDolarFormat(d.q17b);
		tooltipdiv.html(textTooltip)
			.style("top", d3.event.pageY - 40 + "px")
			.style("left", d3.event.pageX + 25 + "px")
		jQuery('.arrow_box').css('display','block');


	}

	function mouseout(){
		//d3.selectAll('.mouseover').classed('mouseover', false)
		jQuery('.arrow_box').css('display','none');
	}

	function sortLegendContent(g){
		//circles
		sortLegend.select('.min circle')
			.attr('cx',scaleCircle(g.value,minValue[g.value]))
			.attr('r',scaleCircle(g.value,minValue[g.value]))
		sortLegend.select('.max circle')
			.attr('cx',scaleCircle(g.value,maxValue[g.value]))
			.attr('r',scaleCircle(g.value,maxValue[g.value]))
		// text
		sortLegend.select('.min text').text(formatSizeValue(minValue[g.value]))
			.attr('x',15)
		sortLegend.select('.max text').text(formatSizeValue(maxValue[g.value]))
			.attr('x',90)

		rearrangeLegendItem('size-legend')

	}

	function rearrangeLegendItem(legendClass){
		var previousWidth = 0;
		d3.selectAll('.'+legendClass+' .legendItem')
			.each(function(d){
				d3.select(this).attr("transform","translate("+previousWidth+",0)")
				var bbox = d3.select(this).node().getBBox();
				previousWidth += Math.round(bbox.width) + 10;
			})
	}
	function formatSizeValue(value){
		if (activeIndicator == '_infeco') return infEconLabels[value];
		else if (activeIndicator == '_exprev' || activeIndicator == 'Voters Turnout') return  +value.toFixed(2)+'%';
		else return value;
	}
	function wrap(text, width) {
	  text.each(function() {
		var text = d3.select(this),
			words = text.text().split(/\s+/).reverse(), //.split(/\s|-/).reverse()
			word,
			line = [],
			lineNumber = 0,
			lineHeight = 1, // ems
			y = text.attr("y"),
			dy = parseFloat(text.attr("dy")),
			tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");

		if (words.length > 1){
			while (word = words.pop()) {
				line.push(word);
				tspan.text(line.join(" "));
			  if (tspan.node().getComputedTextLength() > width){
				line.pop();
				tspan.text(line.join(" "));
				line = [word];
				tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em")
				.attr('class','line-'+lineNumber).text(word);
			  }
			}
		}
		// no wrapping in case the text has only one word even when it's too long
		else tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", "0em").text(words.pop());
	  });
	}

	jQuery('#subitem-colour').hide();

	jQuery('#mainitem-colour').click(function() {
		jQuery('#subitem-colour').slideToggle()
	})

	jQuery('#subitem-sort').hide();

	jQuery('#mainitem-sort').click(function() {
		jQuery('#subitem-sort').slideToggle()
	})


	function numberWithCommasDolarFormat(x) {
		return '$'+x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
}

d3.selection.prototype.size = function() {
    var n = 0;
    this.each(function() { ++n; });
    return n;
  };

/*$(document).ready(function(){
    $(this).scrollTop(0);
});*/

function loadVis(){
	var url = '/app/themes/urbangovernance/';
	//var url ='';

  var data_package_id = getParameterByName('dp_id');
  var data_package_environment = getParameterByName('dp_env');
  var api_query;

  if(data_package_id && data_package_id.length > 0) {
    api_query = '/api/v0/data-package/by-id/' + data_package_id;
  } else if (data_package_environment && data_package_environment.length > 0 ){
    api_query = '/api/v0/data-package/by-environment/' + data_package_environment;
  } else {
    api_query = '/api/v0/data-package/by-environment/production';
  }

  d3.json(api_query, function(error, data) {
		if(!error) {
      queue()
  			.defer(d3.csv, data.survey_results_data)
        .await(displayCities);
    } else {
      queue()
        .defer(d3.csv, url + "data/SurveyResults_Numericaldata.csv")
        .await(displayCities);
    }
	});
}

/**
 * @x-technical-debt: add credits
 * @x-technical-debt: refactor to module and bundle once with all scripts
 */
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
