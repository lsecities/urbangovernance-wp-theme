  // single object with all variables that require translations
  var language_data = {
    sortByOptions: [
      {
        id: 'exp-rev',
        name: {
          'en-GB': 'Revenue vs Expenditure',
          'es-ES': 'Ingreso vs Gasto',
          'fr-FR': 'Revenus vs. Dépenses'
        },
        value: '_exprev'
      },
      {
        id: 'inf-eco',
        name: {
          'en-GB': '% of informal economy',
          'es-ES': '% de la economía informal',
          'fr-FR': '% économie informelle'
        },
        value: '_infeco'
      },
      {
        id: 'num-pol',
        name: {
          'en-GB': 'No. of policy sectors led by the city',
          'es-ES': 'Núm. de sectores de política pública liderados por la ciudad',
          'fr-FR': 'No de politiques sectorielles dirigées par la ville'
        },
        value: 'Policy sectors count'
      },
      {
        id: 'loc-pol',
        name: {
          'en-GB': 'No. of participatory mechanisms available to citizens',
          'es-ES': 'Núm. de mecanismos de participación a disposición de los ciudadanos',
          'fr-FR': 'No de mécanismes de participation disponibles pour les citoyens'
        },
        value: 'Local policies count'
      },
      {
        id: 'voter',
        name: {
          'en-GB': 'Voters Turnout (latest election)',
          'es-ES': 'Participación electoral (última elección)',
          'fr-FR': 'Taux de participation électorale (dernière élection)'
        },
        value: 'Voters Turnout (latest election)'
      }
    ],
    colourByOptions: [
      { id: 'reg',
        name: {
          'en-GB': 'World Region',
          'es-ES': 'Región del Mundo',
          'fr-FR': 'Région du monde'
        },
        value:'UN Region Level 2'
      },
      {
        'id':'pop',
        'name': {
          'en-GB': 'Population',
          'es-ES': 'Población',
          'fr-FR': 'Population'
        },
        value:'Population'
      },
      {
        'id':'gdp',
        'name': {
          'en-GB': 'Wealth',
          'es-ES': 'Riqueza',
          'fr-FR': 'Richesse'
        },
        value:'GDP/Capita (per country) 2013 - World Bank Data'
      }
    ],
    income_levels: {
      'en-GB': [ 'low income', 'middle income', 'high income' ],
      'es-ES': [ 'ingreso bajo', 'ingreso medio', 'ingreso alto' ],
      'fr-FR': [ 'bas revenus', 'revenus moyens', 'haut revenus' ]
    },
    continent_colors: {
      'Africa': {
        name: {
          'en-GB': 'Africa',
          'es-ES': 'África',
          'fr-FR': 'Afrique'
        },
        color: '#D97196',
      },
      'Latin America and the Caribbean': {
        name: {
          'en-GB': 'Latin America and the Caribbean',
          'es-ES': 'América Latina y el Caribe',
          'fr-FR': 'Amerique Latine et Caraibes'
        },
        color: '#F69A4D',
      },
      'Asia': {
        name: {
          'en-GB': 'Asia',
          'es-ES': 'Asia',
          'fr-FR': 'Asie'
        },
        color: '#8781A7',
      },
      'Europe': {
        name: {
          'en-GB': 'Europe',
          'es-ES': 'Europa',
          'fr-FR': 'Europe'
        },
        color: '#67B064',
      },
      'Northern America': {
        name: {
          'en-GB': 'Northern America',
          'es-ES': 'Norteamérica',
          'fr-FR': 'Amerique du Nord'
        },
        color: '#3393C1',
      },
      'Oceania': {
        name: {
          'en-GB': 'Oceania',
          'es-ES': 'Oceanía',
          'fr-FR': 'Oceanie'
        },
        color: '#FF0009',
      }
    },
    strings: {
      'en-GB': {
        no_answer: 'Unavailable ',
        population: 'Population',
        show_legend: 'Show Legend',
        hide_legend: 'Hide Legend',
        people_working_for_the_city_local_government: 'People working for the city/local government',
        revenue: 'Revenue',
        expenditure: 'Expenditure',
        highlight_cities: 'Highlight cities',
        colour_by: 'Colour by:',
        sort_by: 'Sort by:',
        cta_scroll_slowly: 'Scroll slowly to see the charts',
        cta_click_on_a_dot: 'Click on a dot for information about a city',
        world_region: 'World region',
        metropolitan_area: 'Metropolitan area',
        governance_challenges: 'Governance challenges',
        print_profile: 'Print city profile'
      },
      'es-ES': {
        no_answer: 'Sin respuesta ',
        population: 'Población',
        show_legend: 'Ver Leyenda',
        hide_legend: 'Esconder Leyenda',
        people_working_for_the_city_local_government: 'Num. de personas empleadas por el gobierno local',
        revenue: 'Ingreso',
        expenditure: 'Gasto',
        highlight_cities: 'Muestra la ciudad',
        colour_by: 'Color:',
        sort_by: 'Ordenar:',
        cta_scroll_slowly: 'Desplazar lentamente hacia abajo para ver los gráficos',
        cta_click_on_a_dot: 'Haga clic en un punto para ver la información de una ciudad',
        world_region: 'Región del Mundo',
        metropolitan_area: 'Área metropolitana',
        governance_challenges: 'Retos de Gobernanza',
        print_profile: 'Imprimir perfil'
      },
      'fr-FR': {
        no_answer: 'Pas de réponse ',
        population: 'Population',
        show_legend: 'Montrer la légende',
        hide_legend: 'Cacher la légende',
        people_working_for_the_city_local_government: 'Nombre de personnes travaillant pour le gouvernement local',
        revenue: 'Revenus',
        expenditure: 'Dépenses',
        highlight_cities: 'Montrer la ville',
        colour_by: 'Couleur:',
        sort_by: 'Trier:',
        cta_scroll_slowly: 'Faites défiler vers le bas pour voir les graphiques',
        cta_click_on_a_dot: 'Cliquer sur les points pour voir les informations sur une ville',
        world_region: 'Région du monde',
        metropolitan_area: 'Metropolitan area',
        governance_challenges: 'Défis de gouvernance',
        print_profile: 'Imprimer le profil'
      },
    },
    influence: {
      'en-GB': {
        influence_0: 'no influence',
        influence_1: 'limited influence',
        influence_2: 'moderate influence',
        influence_3: 'significant influence'
      },
      'es-ES': {
        influence_0: 'sin influencia',
        influence_1: 'influencia limitada',
        influence_2: 'influencia moderada',
        influence_3: 'influencia importante' 
      },
      'fr-FR': {
        influence_0: "pas d'influence",
        influence_1: 'influence limitée',
        influence_2: 'influence modérée',
        influence_3: 'influence importante'
      },
    },
    profile:{
      'en-GB': 'City profile',
      'es-ES': 'Perfil de la ciudad',
      'fr-FR': 'Profil de la ville'
    },
    profile_page_path: {
      'en-GB': 'cities-profiles/city-profile',
      'es-ES': 'perfiles-de-las-ciudades/informe-de-la-ciudad',
      'fr-FR': 'profils-des-villes/rapport-de-la-ville'
    }
  };

//module.exports = language_data;
