var displayCitiesList = function(error, surveyData){
  // get current page language from the lang attribute of the html
  // element (this is set by the polylang WP plugin); default is en-GB
	var current_language = jQuery('html').attr('lang') || 'en-GB';
  	//console.log('error', error)
	// single object with all variables that require translations

     var language_data = {
     	profile_page_name: {
		  'en-GB': 'city-profile',
		  'es-ES': 'informe-de-la-ciudad',
		  'fr-FR': 'rapport-de-la-ville'
		}
    }


	surveyData.sort(function(obj1, obj2) {
		var textA = obj1.q01x;
		var textB = obj2.q01x;
		return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
	});


	var citylist = d3.select("#list")
		.append('div')
		.attr('class','row')
		.selectAll('.city-link')
		.data(surveyData)
		.enter()
		.append('div')
		.attr('class','city-link')
		.html(function(d){
			return '<a href= "'+language_data.profile_page_name[current_language]+'/?city='+d['city id']+'">'+ d.q01x + '/ <span class="country-name">' + d.q01+ '</span></a>';
		})

	function formatSizeValue(value){
		if (activeIndicator == '_infeco') return infEconLabels[value];
		else if (activeIndicator == '_exprev' || activeIndicator == 'Voters Turnout (latest election)') return  +value.toFixed(2)+'%';
		else return value;
	}
	function wrap(text, width) {
	  text.each(function() {
		var text = d3.select(this),
			words = text.text().split(/\s+/).reverse(), //.split(/\s|-/).reverse()
			word,
			line = [],
			lineNumber = 0,
			lineHeight = 1, // ems
			y = text.attr("y"),
			dy = parseFloat(text.attr("dy")),
			tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");

		if (words.length > 1){
			while (word = words.pop()) {
				line.push(word);
				tspan.text(line.join(" "));
			  if (tspan.node().getComputedTextLength() > width){
				line.pop();
				tspan.text(line.join(" "));
				line = [word];
				tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em")
				.attr('class','line-'+lineNumber).text(word);
			  }
			}
		}
		// no wrapping in case the text has only one word even when it's too long
		else tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", "0em").text(words.pop());
	  });
	}


}

d3.selection.prototype.size = function() {
    var n = 0;
    this.each(function() { ++n; });
    return n;
  };


function loadVis(){
	var url = '/app/themes/urbangovernance/';
	//var url ='';

	var data_package_id = getParameterByName('dp_id');
  var data_package_environment = getParameterByName('dp_env');
  var api_query;

  if(data_package_id && data_package_id.length > 0) {
    api_query = '/api/v0/data-package/by-id/' + data_package_id;
  } else if (data_package_environment && data_package_environment.length > 0 ){
    api_query = '/api/v0/data-package/by-environment/' + data_package_environment;
	} else {
    api_query = '/api/v0/data-package/by-environment/production';
  }

  d3.json(api_query, function(error, data) {
		if(!error) {
      queue()
  			.defer(d3.csv, data.survey_results_data)
        .await(displayCitiesList);
    } else {
      queue()
        .defer(d3.csv, url + "data/SurveyResults_Numericaldata.csv")
        .await(displayCitiesList);
    }
	});
}

/**
 * @x-technical-debt: add credits
 * @x-technical-debt: refactor to module and bundle once with all scripts
 */
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
