function sortAnswersByRelevance(rawTitlesData, rawSurveyData, questionsForAutomaticSorting) {
  // If no sorting configuration data is provided, just return the raw data
  if(questionsForAutomaticSorting === null || questionsForAutomaticSorting.length === 0) return [rawTitlesData, rawSurveyData];

  var alpha = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

  var surveyData_copy = rawSurveyData;
  var titlesData_copy = rawTitlesData;

  questionsForAutomaticSorting.forEach(function(question) {
    var answer_weight, sorted_rows;

    // regexp used to match answer codes (in surveyData)
    var re = new RegExp('^' + question.question_id);

    // extract list of 'answers' (columns and rows) for this question
    var answers_matrix = rawTitlesData.map(function(group) {
      return group.questions.filter(function(item) {
        return item.qcode === question.question_id;
      });
    })
    .filter(function(item) { return item.length; })[0][0].answers;

    var rows = answers_matrix.filter(function(item) {
      return item.type === 'row';
    })
    .sort(function(a, b) {
      return a.position - b.position;
    });

    var columns = answers_matrix.filter(function(item) {
      return item.type === 'col';
    });

    var question_data = rawSurveyData.map(function(item) {
      return Object.keys(item).filter(function(key) {
        return key.match(re);
      })
      .map(function(q) {
        return item[q];
      });
    });

    // calculate counts of cities for each columns of each row,
    // associating the sum to the original position and sorting
    // @x-technical-debt: add secondary sorting by original position (if count is equal)
    // @x-technical-debt: add ability to sort descending
    answer_weight = rows.map(function(item, index) {
      /**
       * matrix->rating and matrix->menu questions (type->subtype) need to
       * be treated differently: rating subtype carry one value per possible
       * answer, whereas menu subtype carry one value per category within
       * each possible answer.
       * in practice, rating subtype questions answer questions such as 'which
       * government tier is responsilble for nurseries? choose *one* of the
       * following (below city, city, metropolitan area, etc.)' whereas
       * menu subtype questions answer questions such as 'for each of the
       * following classes of actors, rate their leadership within urban
       * transport, on a scale from 0 to 4'.
       * in practice, for all questions of subtype menu, cities are only
       * put on the respective chart if the answer is 4 (i.e. highly relevant),
       * but in theory this could be changed to include e.g. 3 and 4, hence the
       * need for an array of values in the configuration of sorting, as
       * explained below.
       * accordingly, 'by' is configured differently:
       * for rating, only the position of the column against which to sort
       * is given; for menu subtype each 'by' item is itself a (two-member)
       * list: the first value is the column against which to sort, and the
       * second member is the list of values that should count as 'relevant'
       * to grant the inclusion of the city in a specific cluster of the
       * matrix chart.
       */
      return question_data.reduce(function(p, c, ci, a) {
        if(Array.isArray(question.by[0])) {
          // if question.by is an array, this is configured as a menu subtype
          // question
          var row_answers = c.slice(index * columns.length, index * columns.length + columns.length);

          var relevant_stuff = question.by.filter(function(by_def) {
            return by_def[1].indexOf(Number(row_answers[by_def[0] - 1])) >=0;
          });
          if(relevant_stuff.length) p.value++;
        } else {
          // otherwise, this is configured as a rating subtype question
          if(question.by.indexOf(c[index].toString()) >= 0) {
            p.value++;
          }
        }

        return p;
      },
      { original_position: index, value: 0 })
    })
    .sort(function(a,b) {
      return b.value - a.value;
    });

    /**
     * now use the position of each row, as sorted above, to reorder the
     * rows
     */
    sorted_rows = answer_weight.map(function(item, index) {
      var answer = rows[item.original_position];
      answer.position = index + 1;
      return answer;
    });

    /**
     * now the order of answers (columns in the raw survey data CSV) needs to
     * be swapped to match the order of rows. again, how this is done depends on
     * whether this is a rating subtype or menu subtype question.
     * for rating subtype questions, reordering is straightforward: there are
     * N answer columns for N rows, with a 1:1 mapping - so the order of answer
     * columns is just mapped 1:1 to the new order of rows.
     */
    if(Array.isArray(question.by[0])) {
      rawSurveyData.forEach(function(item, index) {
        answer_weight.forEach(function(it, id) {
          columns.forEach(function(col_it, col_id){
            // poor man's sprintf to generate a left-zero-padded string
            // representation of the index of the current row (01, 02, etc.)
            var padded_row_id = "0" + (id + 1);
            padded_row_id = padded_row_id.substr(-2);

            surveyData_copy[index][question.question_id + padded_row_id + alpha[col_id]] =
              question_data[index][it['original_position'] * columns.length + col_id];
          });
        })
      });
    } else {
    /**
     * for *menu* subtype questions, reordering requires swapping blocks of
     * N*(question columns) answer columns [question columns are the
     * columns appearing in the visualization for this question, answer columns
     * are those in the CSV raw data - just stay with me here]
     * e.g., for *menu* subtype questions which have 4 possible categories over
     * which to rate the (leadership, etc.) of a city within each area of
     * concern, columns of the original CSV dataset will have to be moved
     * four at a time.
     */
      rawSurveyData.forEach(function(item, index) {
        answer_weight.forEach(function(it, id) {
          surveyData_copy[index][question.question_id + alpha[id]] = question_data[index][it['original_position']];
        });
      });
    }

    /**
     * and finally, populate a copy of the groups ('pages') of answer categories
     * according to the new order of categories
     */
    rawTitlesData.forEach(function(group, group_index) {
      group.questions.forEach(function(group_question, group_question_index) {
        if(group_question.qcode === question.question_id) group_question.answers = sorted_rows.concat(columns);
        titlesData_copy[group_index][group_question_index] = group_question;
      });
    });
  });

  return [ titlesData_copy, surveyData_copy ];
}

var scrollVis = function(current_language, borders, rawSurveyData, rawTitlesDataFull, shortQuestions, rawHeight, questionsHeight) {

  var breakpoint = 992; //used for responsiveness
  var margin = {
    top: 50,
    left: 0,
    bottom: 20,
    right: 0
  };
  var height = rawHeight - margin.top - margin.bottom;
  var width = jQuery('#vis').width();
  var visWidth = (width >= breakpoint) ? width * 0.75 : width;
  var verticalTranslation = (width >= breakpoint) ? margin.top : 10;
  var scale = width * 140 / 878;
  var first = true; // first time loading for animation cities on map
  var highlightCount = 0;
  var dotsTranslationValue = (width <= breakpoint) ? 10 : width * .25;
  var labelsTranslationValue = (width <= breakpoint) ? 20 : width * .25 + 10;
  // select portion of titlesDataFull structure for the current language
  var rawTitlesData = rawTitlesDataFull[current_language];

  // Keep track of which visualization
  // we are on and which was the last
  // index activated. When user scrolls
  // quickly, we want to call all the
  // activate functions that they pass.
  var lastIndex = -1;
  var activeIndex = 0;

  var activeQuestion = '';
  var activeColour = 'reg'; //'UN Region Level 2';

  var cityCircles, borders, cityInfo, cityData;
  var radiusCircle = 5;
  var circlePad = 2;
  var qRadiusCircle = {};
  var qCountArray = {};

  var delayCircles = 10;
  var delayQCircles = 7;
  var delayLabels = 100;
  var delayAmount = 20;

  var opacityMap = .4;
  var opacityUnselected = 1;
  var removeDuration = 100;

  var highlightIndex = 0;
  var highlightIndexId = {};

  var continentColors = language_data.continent_colors;

  var populationColors = ['#ffCCCD', '#ff7f84', '#ff0009', '#B20006', '#7f0004']

  var populationQuantile = d3.scale.quantile()
    .domain([0, 500000, 1000000, 5000000, 10000000, 100000000])
    .range(populationColors)

  var wealthColors = ['#ff0009', '#ff8504', '#fac808'].reverse();

  var wealthQuantile = d3.scale.quantile()
    .domain([0, 2000, 20000, 100000000])
    .range(wealthColors)

  var alpha = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']; //for matrix

  var questions = [];
  shortQuestions.forEach(function(d) {
    questions.push(d.qcode.trim())
  });

  // this is used for matrix chart which needs to be divided in sections

  var questionGroups = {
    'q09': [0, 9],
    'q13': [0, 4],
    'q28a': [0, 5],
    'q28b': [5, 9],
    'q28c': [9, 13],
    'q28d': [13, 17],
    'q29a': [0, 4],
    'q29b': [4, 8],
    'q29c': [8, 11],
    'q30a': [0, 5],
    'q30b': [5, 10],
    'q30c': [10, 14],
    'q34a': [0, 5],
    'q34b': [5, 10],
    'q34c': [10, 15],
    'q34d': [15, 20],
    'q38a': [0, 5],
    'q38b': [5, 10],
    'q38c': [10, 15],
    'q38d': [15, 19],
    'q41a': [0, 5],
    'q41b': [5, 10],
    'q41c': [10, 15],
    'q41d': [15, 20]
  };

  var questionsForAutomaticSorting = [
    {
      "question_id": "q28",
      "by": [ "2" ]
    },
    {
      "question_id": "q29",
      "by": [ [ 1, [ 4 ] ] ]
    },
    {
      "question_id": "q30",
      "by": [ "4" ]
    },
    {
      "question_id": "q34",
      "by": [ "5" ]
    },
    {
      "question_id": "q38",
      "by": [ [ 2, [ 4 ] ] ]
    },
    {
      "question_id": "q41",
      "by": [ [ 2, [ 4 ] ] ]
    }
  ];

  var calculatedQuestions = []; //this array will store the questions from which the dot coordinates have already been calculated
  var titles = [];
  var shortquestions = [];

  /**
   * Reorder answers for all the questions listed in questionsForAutomaticSorting
   */
  var sortedData = sortAnswersByRelevance(rawTitlesData, rawSurveyData, questionsForAutomaticSorting);
  // and assign the resulting objects back to where the stock code expects
  // to find them
  var titlesData = sortedData[0];
  var surveyData = sortedData[1];

  titlesData.forEach(function(d) {
    d.questions.forEach(function(g) {
      if (g.qcode) {
        titles.push(g); // push only questions for which
      }
    })
  })

  var titlesByCode = d3.map(titles, function(d) {
    return d.qcode
  })
  var surveyDataByCity = d3.map();

  var projection = d3.geo.equirectangular()
    .scale(scale)
    .rotate([-10, 0])
    .translate([width / 2, height / 2 + 40])
    .precision(.1);

  var path = d3.geo.path()
    .projection(projection);

  var tooltipdiv = d3.select("body")
    .append("div")
    .attr("class", "arrow_box");

  var legenddiv = d3.select("#vis")
    .append("div")
    .attr("class", "legenddiv")
    .append('div')
    .attr('class', 'legendtoggle');

  // set translated text for UI elements that have text set in HTML markup
  d3.select('.colourby')
    .html(language_data.strings[current_language].colour_by);
  d3.select('#mainitem .selected-item')
    .html(language_data.colourByOptions.filter(function(i) {
      return i.id === 'reg';
    })[0].name[current_language]);
  d3.select('#sections ol li:nth-child(1)')
    .html(language_data.strings[current_language].cta_scroll_slowly);
  d3.select('#sections ol li:nth-child(2)')
    .html(language_data.strings[current_language].cta_click_on_a_dot);

  var themetitle = d3.select("#vis")
    .append("div")
    .attr('class', 'theme-title col-md-7 col-lg-9')

  d3.select('.legenddiv').append('div')
    .attr('id', 'legendclose')
    .text(language_data.strings[current_language].show_legend)

  // continent legend
  var continentLegend = legenddiv.append('div')
    .attr('class', 'reg-legend legend')
    .append('svg')
    .append('g')
    .attr("transform", "translate(0,18)")
    .selectAll('.legendItem')
    .data(d3.entries(continentColors))
    .enter()
    .append("g")
    .attr("transform", function(d, i) {
      return "translate(0," + i * 18 + ")"
    })
    .attr('class', 'legendItem')

  continentLegend.append('circle')
    .attr('cx', 10)
    .attr('cy', -5)
    .attr('fill', function(d) {
      return d.value.color
    })
    .attr('r', 5)

  continentLegend.append('text')
    .attr('x', 22)
    .style("text-anchor", 'start')
    .text(function(d) {
      return d.value.name[current_language]
    })

  // population legend
  var populationLegend = legenddiv.append('div')
    .attr('class', 'pop-legend legend')
    .append('svg')
    .append('g')
    .attr("transform", "translate(0,20)")
    .selectAll('.legendItem')
    .data([' < 500,000', '500,000 - 1,000,000', '1,000,000 - 5,000,000', '5,000,000 - 10,000,000', ' >  10,000,000'])
    .enter()
    .append("g")
    .attr("transform", function(d, i) {
      return "translate(0," + i * 18 + ")"
    })
    .attr('class', 'legendItem')

  populationLegend.append('circle')
    .attr('cx', 10)
    .attr('cy', -5)
    .attr('fill', function(d, i) {
      return populationColors[i]
    })
    .attr('r', 5)

  populationLegend.append('text')
    .attr('x', 22)
    .style("text-anchor", 'start')
    .text(function(d) {
      return d
    })

  legenddiv.select('.pop-legend').style('display', 'none')


  // wealth legend
  var wealthLegend = legenddiv.append('div')
    .attr('class', 'gdp-legend legend')
    .append('svg')
    .append('g')
    .attr("transform", "translate(0,20)")
    .selectAll('.legendItem')
    .data(language_data.income_levels[current_language])
    .enter()
    .append("g")
    .attr("transform", function(d, i) {
      return "translate(0," + i * 22 + ")"
    })
    .attr('class', 'legendItem')

  wealthLegend.append('circle')
    .attr('cx', 10)
    .attr('cy', -5)
    .attr('fill', function(d, i) {
      return wealthColors[i]
    })
    .attr('r', 5)

  wealthLegend.append('text')
    .attr('x', 22)
    .style("text-anchor", 'start')
    .text(function(d) {
      return d
    })

  legenddiv.select('.gdp-legend').style('display', 'none')

  var svg = null;
  var g = null;
  var labelsGroup = null;
  var qCircles = null;

  var citySelected, citySelectedId;

  //change array depending on language
  var colourByOptions = language_data.colourByOptions;

  d3.select('#subitem ul').selectAll('li')
    .data(colourByOptions)
    .enter()
    .append('li')
    .attr('id', function(d) {
      return d.id
    })
    .text(function(d) {
      return d.name[current_language]
    })
    .on('click', colourBy)

  // When scrolling to a new section
  // the activation function for that
  // section is called.
  var activateFunctions = [];
  // If a section has an update function
  // then it is called while scrolling
  // through the section with the current
  // progress through the section.
  var updateFunctions = [];

  var chart = function(selection) {
    selection.each(function() {
      svg = d3.select("#vis").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", rawHeight)
        .append('g')
        .attr('transform', "translate(" + margin.left + ',' + verticalTranslation + ")")
        .style("display", "block")
        .style("margin", "auto")

      gBorders = svg.append("g").attr('class', 'borders')
      g = svg.append("g").attr('class', 'dots')

      labelsGroup = svg.append('g').attr('class', 'labels')
        .attr("transform", "translate(" + labelsTranslationValue + ",5)")

      setupVis();
      setupSections();
    });
  };

  var allCityNames = [];

  //initializing different values for the cities
  surveyData.forEach(function(d, i) {
    d.x = {};
    d.y = {};
    d.radius = {};
    d._id = i;
    allCityNames.push({
      'name': d.q01x,
      'id': i
    })
  })

  surveyData.sort(function(obj1, obj2) {
    var textA = obj1['UN Region Level 2'].toUpperCase();
    var textB = obj2['UN Region Level 2'].toUpperCase();
    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
  });

  setupVis = function() {

    surveyDataByCity = d3.map(surveyData, function(d) {
      return d.q01x
    })

    borders = gBorders.selectAll(".borders")
      .data(borders.features)
      .enter()
      .append("path")
      .attr("class", "borders")
      .attr("d", path)
      .style("opacity", 0);

    cityCircles = g.selectAll("city")
      .data(surveyData)
      .enter().append("circle")
      .attr('id', function(d) {
        return 'city_' + d._id
      })
      .attr('class', 'citycircle')
      .attr('cx', function(d) {
        return projection([d.POINT_X, d.POINT_Y])[0]
      })
      .attr('cy', function(d) {
        return projection([d.POINT_X, d.POINT_Y])[1]
      })
      .attr('r', 0)
      .attr('fill', function(d) {
        return circleColour(d)
      })
      .on("mouseover", mouseover)
      .on("mouseout", mouseout)
      .on("click", function(d) {
        selectCity(d)
      })

    cityCircles.transition()
      .delay(function(d, i) {
        return i * 20
      })
      .attr('r', 10)
      .transition()
      .attr('r', 4)

    cityInfo = d3.select('#city-info')
    cityData = cityInfo.select('.city-data')

  }

  setupSections = function() {

    // setting up text area
    var section = d3.select('#sections').selectAll()
      .data(shortQuestions)
      .enter()
      .append('section')
      .attr('id', function(q) {
        return 'section' + q.qcode
      })
      .attr('class', 'step')
      .html(function(q) {
        return q['shortquestion_' + current_language];
      })
      .on('click', function(q) {
        jQuery('html, body').animate({
          scrollTop: jQuery("#section" + q.qcode).offset().top - questionsHeight
        }, 500);
      })

    d3.select('#sections').append('div').attr('id', 'extra-space')

    activateFunctions[0] = showMap;
    count = 1;
    shortQuestions.forEach(function(d) {
      var q = d.qcode;
      var chart = titlesByCode.get(q.substring(0, 3)).type.family;
      var subtype = titlesByCode.get(q.substring(0, 3)).type.subtype;

      // only log question metadata if we are in 'development' data package environment (?dp_env=development query variable)
      if(getParameterByName('dp_env') === 'development') {
        console.log(q + ': ' + d['theme_en-GB'] + ' - '+ d['shortquestion_en-GB'] + ': ' + chart + '|' + subtype);
      }

      activateFunctions[count] = function() {
        first = false;
        switch (chart) {
          case "single_choice":
            showSingleChoice(q);
            break;
          case "multiple_choice":
            showMultipleChoice(q);
            break;
          case "matrix":
            if (subtype == "multi" || subtype == "menu") showGroupsMatrix(q, subtype);
            else if (subtype == "single" || subtype == "rating") showMatrixSingle(q, subtype);
            break;
            /*default:
            	console.log('unknown kind of chart', q, chart);	*/
        }
      }
      count++;
    });

    for (var i = 0; i < count; i++) {
      updateFunctions[i] = function() {};
    }
  }

  function showMap() {
    activeQuestion = 'q00';
    borders.transition().style('opacity', 1)
    labelsGroup.selectAll("*").style('opacity', 0).remove()

    if (!first) {
      cityCircles.style('opacity', 1)
        .transition()
        .ease('cubic')
        .delay(function(d, i) {
          return i * delayCircles
        })
        .attr('transform', 'translate(0)')
        .attr('cx', function(d) {
          return projection([d.POINT_X, d.POINT_Y])[0]
        })
        .attr('cy', function(d) {
          return projection([d.POINT_X, d.POINT_Y])[1]
        })
        .attr('r', 3)
    }
  }
  /*---- single choice functions --- */
  function showSingleChoice(q) {

    activeQuestion = q;
    borders.transition().style('opacity', opacityMap)
    if (qCircles) qCircles.transition().duration(removeDuration).style('opacity', 0).remove()

    var titles = titlesByCode.get(q).answers.filter(function(d) {
      if (d.position) return true
    }).sort(function(a, b) {
      return a.position - b.position
    })
    var n = titles.length;
    var domain = [];

    for (var i = 0; i < n; i++) {
      domain.push(i + 1)
    }

    var y = d3.scale.ordinal()
      .domain(domain)
      .rangeRoundBands([0, height - 20], .4);

    showLabelsVertically(q, titles, n, y.range());
    positionCirclesY(q, n, y.range());

    cityCircles.attr('transform', 'translate(' + dotsTranslationValue + ',10)')
      .style('opacity', function(d) {
        return (d._id == citySelectedId) ? 1 : opacityUnselected
      })
      .transition()
      .ease('cubic')
      .delay(function(d, i) {
        return i * delayCircles
      })
      .attr('cx', function(d) {
        return d.x[q]
      })
      .attr('cy', function(d) {
        return d.y[q]
      })
      .attr('r', radiusCircle)

  }

  function showLabelsVertically(q, titles, n, yRange) {
    labelsGroup.selectAll("*").style('opacity', 0).remove()

    labelsGroup.selectAll(".label")
      .data(titles)
      .enter()
      .append('text')
      .filter(function(d) {
        if (d.position) return d
      })
      .attr('class', 'label')
      .attr('y', 5)
      .attr("text-anchor", "end")
      .attr("transform", function(d, i) {
        return "translate(205," + (yRange[i] + 5) + ")"
      })
      .attr('dx', 0)
      .attr('dy', 0)
      .text(function(d) {
        return d.text
      })
      .call(wrap, 200)
      .attr('opacity', 0)
      .transition()
      .delay(function(d, i) {
        return i * delayLabels
      })
      .attr('opacity', 1)

    labelsGroup.selectAll('line')
      .data(titles)
      .enter()
      .append('line')
      .attr('class', 'axis-line')
      .attr('x1', 215)
      .attr('x2', 215)
      .attr('y1', 5)
      .attr('y2', height - 30)

    labelsGroup.append('text')
      .attr('class', 'label-single-horizontal')
      .attr('y', height - 5)
      .attr('x', 0)
      .attr("text-anchor", "start")
      .text(language_data.strings[current_language].no_answer)

  }

  function positionCirclesY(q, n, yRange) {
    if (n > 6) circlePerRow = 2;
    else circlePerRow = 4;

    if (calculatedQuestions.indexOf(q) < 0) {
      calculatedQuestions.push(q);

      qCountArray[q] = [];
      for (var i = 0; i < n; i++) {
        qCountArray[q][i] = 0;
      }

      var count = 0;
      surveyData.forEach(function(d) {
        cityResults = surveyDataByCity.get(d.q01x)
        var resultValue = cityResults[q];
        if (resultValue > 0) {
          d.y[q] = (qCountArray[q][resultValue - 1] % circlePerRow) * (radiusCircle * 2 + circlePad) + yRange[resultValue - 1];
          d.x[q] = 240 + Math.floor(qCountArray[q][resultValue - 1] / circlePerRow) * (radiusCircle * 2 + circlePad);
          qCountArray[q][resultValue - 1] += 1;
        } else {
          d.x[q] = count * (radiusCircle * 2 + circlePad) + 120;
          d.y[q] = height - 13;
          count += 1;
        }
      })
    }

    labelsGroup.append('g').selectAll('.amount')
      .data(qCountArray[q])
      .enter()
      .append('text')
      .attr('class', 'amount')
      .attr('y', function(d, i) {
        return yRange[i] + 12
      })
      .attr('x', function(d) {
        return Math.ceil(d / circlePerRow) * (radiusCircle * 2 + circlePad) + 230
      })
      .text(function(d) {
        return d
      })
  }

  /*---- multiple choice functions --- */
  function showMultipleChoice(q) {
    activeQuestion = q;
    if (qCircles) qCircles.transition().duration(removeDuration).style('opacity', 0).remove()

    var titles = titlesByCode.get(q).answers.filter(function(d) {
      if (d.position) return true
    }).sort(function(a, b) {
      return a.position - b.position
    })
    var n = titles.length;

    var domain = [];

    for (var i = 0; i < n; i++) {
      domain.push(i + 1)
    }

    var x = d3.scale.ordinal()
      .domain(domain)
      .rangeRoundBands([0, width], .4);

    var y = d3.scale.ordinal()
      .domain(domain)
      .rangeRoundBands([0, height - 20], .4);

    showLabelsVertically(q, titles, n, y.range());
    positionCirclesMultipleChoiceY(q, n, y.range());

    // transition in two parts move circles to q position then show the multiple and hide the city circles
    cityCircles.style('opacity', function(d) {
        return (d._id == citySelectedId) ? 1 : opacityUnselected
      })
      .transition()
      .ease('cubic')
      .delay(function(d, i) {
        return i * delayCircles
      })
      .attr('cx', function(d) {
        return d.x[q]
      })
      .attr('cy', function(d) {
        return d.y[q]
      })
      .attr('r', radiusCircle)

    qCircles.selectAll('circle')
      .transition()
      .delay(function(d, i) {
        return i * delayQCircles
      })
      .style('opacity', function(d) {
        if (citySelectedId) {
          var id = jQuery(this).prop('id');
          id = Number(id.slice(5))
          return (id == citySelectedId) ? 1 : opacityUnselected;
        } else return 1;
      })

    qCircles.selectAll('#city_' + citySelectedId)
      .classed('selected', true)

    // highlighting cities
    highlightClonedCircles();

  }

  function positionCirclesMultipleChoiceY(q, n, yRange) {
    var countArray = [];
    var resultValue;
    if (yRange.length > 9) var circlePerRow = 2;
    else var circlePerRow = 3;
    var count = 0;

    for (var i = 0; i < n; i++) {
      countArray[i] = 0;
    }

    qCircles = svg.append('g')
      .attr('class', q)
      .attr('transform', 'translate(' + dotsTranslationValue + ',10)')

    surveyData.forEach(function(d) {
      var cloneCount = 0;
      resultValue = 0;
      cityResults = surveyDataByCity.get(d.q01x)

      for (var i = 0; i < n; i++) {
        qValue = q + alpha[i]

        if (cityResults[qValue]) {
          resultValue = i + 1;
          if (cloneCount == 0) {
            if (!d.y[q]) { // checking if coordinates have already been calculated
              d.y[q] = (countArray[resultValue - 1] % circlePerRow) * (radiusCircle * 2 + circlePad) + yRange[resultValue - 1];
              d.x[q] = 240 + Math.floor(countArray[resultValue - 1] / circlePerRow) * (radiusCircle * 2 + circlePad)
            }
          } else {
            var newCircle = qCircles.append('circle')
              .attr('class', 'q_circle')
              .attr('id', 'city_' + d._id)
              .attr('r', radiusCircle)
              .attr('fill', circleColour(d))
              .on("mouseover", function() {
                mouseover(d)
              })
              .on("mouseout", mouseout)
              .on("click", function() {
                selectCity(d)
              })
              .attr('cy', (countArray[resultValue - 1] % circlePerRow) * (radiusCircle * 2 + circlePad) + yRange[resultValue - 1])
              .attr('cx', 240 + Math.floor(countArray[resultValue - 1] / circlePerRow) * (radiusCircle * 2 + circlePad))
              .style('opacity', 0)
          }
          countArray[resultValue - 1] += 1;
          cloneCount += 1;
        }
      };
      if (cloneCount == 0) {
        d.x[q] = count * (radiusCircle * 2 + circlePad) + 120;
        d.y[q] = height - 13;

        count += 1;
      }
    })
    d3.select('.labels').append('g').selectAll('.amount')
      .data(countArray)
      .enter()
      .append('text')
      .attr('class', 'amount')
      .attr('y', function(d, i) {
        return yRange[i] + 11
      })
      .attr('x', function(d) {
        return Math.ceil(d / circlePerRow) * (radiusCircle * 2 + circlePad) + 225
      })
      .text(function(d) {
        return d
      })
      /*.attr('opacity',0)
      .transition()
      .delay(function(d, i){ return i * delayLabels })
      .attr('opacity',1)*/
  }


  /*---- Matrix menu/multi functions --- */

  function showGroupsMatrix(q, subtype) {
    activeQuestion = q;
    if (qCircles) qCircles.transition().duration(removeDuration).style('opacity', 0).remove()

    var xdomain = [],
      ydomain = [];
    var qRaw = q.substring(0, 3);

    var allTitles = titlesByCode.get(qRaw).answers;

    var rowTitles = allTitles.filter(function(d) {
      return d.type == "row"
    }).sort(function(a, b) {
      return a.position - b.position
    }).slice(questionGroups[q][0], questionGroups[q][1]);
    var colTitles = allTitles.filter(function(d) {
      return d.type == "col"
    }).sort(function(a, b) {
      return a.position - b.position
    });

    if (q == 'q13') {
      colTitles.pop();
    }

    var nRow = rowTitles.length;
    var nCol = colTitles.length;

    for (var i = 0; i < nCol; i++) {
      xdomain.push(i + 1)
    }
    for (var i = 0; i < nRow; i++) {
      ydomain.push(i + 1)
    }

    if (subtype == 'multi') var paddingTop = 30;
    else paddingTop = 50;

    var x = d3.scale.ordinal()
      .domain(xdomain)
      .rangeRoundBands([0, (visWidth - 150)], .2);

    var y = d3.scale.ordinal()
      .domain(ydomain)
      .rangeRoundBands([0, (height - paddingTop)], .2);

    positionCirclesMatrix(q, nRow, nCol, x.range(), y.range(), subtype)
    showLabelsMatrix(q, rowTitles, colTitles, nRow, nCol, x.range(), y.range(), subtype)
      // transition in two parts move circles to q position then show the multiple and hide the city circles
    cityCircles.style('opacity', 1)
      .transition()
      .ease('cubic')
      .delay(function(d, i) {
        return i * delayCircles
      })
      .attr('r', radiusCircle)
      .attr('cx', function(d) {
        return d.x[q]
      })
      .attr('cy', function(d) {
        return d.y[q]
      })
      .style('opacity', 0)

  }

  function showLabelsMatrix(q, rowTitles, colTitles, nRow, nCol, xRange, yRange, subtype) {

    labelsGroup.selectAll("*").style('opacity', 0).remove()

    if (subtype == 'multi') {
      var paddingLeft = 290;
      var distanceHor = 200;
      var paddingTop = -5;
    } else {
      var paddingLeft = 175; //(q.substring(0,3) == 'q29')? 175:185;
      var distanceHor = (xRange.length > 1) ? xRange[1] - xRange[0] : visWidth / 2;
      var paddingTop = 15;
    }

    var distanceVer = yRange[1] - yRange[0];

    labelsGroup.append('g').selectAll(".label")
      .data(rowTitles)
      .enter()
      .append('text')
      .attr('class', 'label')
      .attr('y', 20)
      .attr("text-anchor", "end")
      .attr("transform", function(d, i) {
        return "translate(140," + ((i + 1) * distanceVer - paddingTop) + ")"
      })
      .attr('dx', 0)
      .attr('dy', 0)
      .text(function(d) {
        return d.text
      })
      .call(wrap, 140)
      .attr('opacity', 0)
      .transition()
      .delay(function(d, i) {
        return i * delayLabels
      })
      .attr('opacity', 1)

    labelsGroup.append('g').selectAll(".label")
      .data(colTitles)
      .enter()
      .append('text')
      .attr('class', 'label')
      .attr('y', 20)
      .attr("text-anchor", "middle")
      .attr("transform", function(d, i) {
        if (colTitles.length == 1) return "translate(" + (distanceHor) + ", 0)";
        else {
          if (subtype == 'multi') return "translate(" + ((i) * distanceHor + paddingLeft) + ", 0)";
          else return "translate(" + ((i) * distanceHor + paddingLeft + 45) + ", 0)";
        }
      })
      .attr('dx', 0)
      .attr('dy', 0)
      .text(function(d) {
        return d.text.replace('<br><br>', '')
      })
      .call(wrap, distanceHor - 10)
      .attr('opacity', 0)
      .transition()
      .delay(function(d, i) {
        return i * delayLabels
      })
      .attr('opacity', 1)

    // drawing axis
    if (subtype == 'multi') labelsGroup.selectAll('.xaxis-line')
      .data(rowTitles)
      .enter()
      .append('line')
      .attr('class', 'xaxis-line')
      .attr('x1', 160)
      .attr('x2', visWidth - 200)
      .attr('y1', function(d, i) {
        return (i + 1) * distanceVer - paddingTop + 20;
      })
      .attr('y2', function(d, i) {
        return (i + 1) * distanceVer - paddingTop + 20;
      })

    else labelsGroup.selectAll('.yaxis-line')
      .data(colTitles)
      .enter()
      .append('line')
      .attr('class', 'yaxis-line')
      .attr("transform", function(d, i) {
        if (colTitles.length == 1) return "translate(" + (distanceHor) + ", 0)";
        else return "translate(" + ((i) * distanceHor + paddingLeft + 45) + ", 0)"
      })
      .attr('x1', 0) //function(d,i){return xRange[i]+ paddingLeft; })
      .attr('x2', 0) //function(d,i){return xRange[i]+ paddingLeft; })
      .attr('y1', 50)
      .attr('y2', height)

  }

  function positionCirclesMatrix(q, nRow, nCol, xRange, yRange, subtype) {

    var countArray = [];
    var resultValue;
    if (subtype == 'multi') {
      var paddingLeft = 300;
      var distanceHor = 200;
      var paddingTop = 50;
    } else {
      var paddingLeft = 230;
      var distanceHor = (xRange.length > 1) ? xRange[1] - xRange[0] : visWidth / 2;
      var paddingTop = 30;
    }

    var distanceVer = yRange[1] - yRange[0];

    var qRaw = q.substring(0, 3);

    for (var i = 0; i < nCol; i++) countArray[i] = 0;

    var circlePerRow = 3;
    var count = 0;

    qCircles = svg.append('g')
      .attr('class', q)
      .attr('transform', 'translate(' + dotsTranslationValue + ',10)')

    var rateGroups = [];

    var max = 0;

    for (var j = 0; j < nCol; j++) {
      for (var i = 1; i <= nRow; i++) {
        var row = (i + questionGroups[q][0]).toString();
        if (row.length < 2) row = '0' + row;
        //creating object which will contain all dots for a certain column/row combination
        var rateGroup = {}
        rateGroup.rowNr = i;
        rateGroup.columnNr = j;
        rateGroup.row = row;
        rateGroup.column = alpha[j];
        rateGroup.values = [];

        qValue = qRaw + row + rateGroup.column;

        var validValue = function(value) {
          if (subtype == 'multi') return (Number(value) > 0); // only for matrix multi
          else if (qRaw == 'q13') return (value == 1); // value for yes
          else return (value == 4); // visualising only highest score
        }

        surveyData.forEach(function(d) {
          var cloneCount = 0;
          cityResults = surveyDataByCity.get(d.q01x)
          if (validValue(cityResults[qValue])) {

            if (cloneCount == 0) {
              if (!d.y[q]) { // checking if coordinates have already been calculated
                d.x[q] = (xRange.length > 1) ? (j * distanceHor + paddingLeft) : distanceHor;
                d.y[q] = i * distanceVer + paddingTop - 20;
              }
            }
            rateGroup.values.push({
              name: d.q01x,
              size: 10
            })
            cloneCount += 1;
          }
        })
        if (rateGroup.values.length > max) max = rateGroup.values.length;
        rateGroups.push(rateGroup)
      }
    }
    cityCoordsCheck(q);
    /// ----
    var min = 20;
    var rad = distanceVer / 2;
    // this is used to scale the rateGroups, the largest group will
    // be as big as the distance between rows (distanceVer)
    // the other will be scale accordingly. Circle areas have been
    // considered to compare rateGroups

    var areaFactor = Math.pow(rad, 2) / max;
    if (areaFactor > 30) areaFactor = 30; // this is to avoid that the circles get too big

    qCircles.selectAll('.rate-groups')
      .data(rateGroups)
      .enter()
      .append('g')
      .attr('class', 'rate-groups')
      .each(function(d) {
        d._side = 2 * Math.sqrt(areaFactor * d.values.length)
        var data = {
          name: "root",
          children: d.values
        }

        var nodes = d3.layout.pack()
          .value(function(d) {
            return d.size;
          })
          .size([d._side, d._side])
          .padding(0)
          .nodes(data)

        nodes.shift();
        if (nodes[0]) var radius = nodes[0].r;
        if (radius < min) min = radius;

        d3.select(this).selectAll('.q_circle')
          .data(nodes)
          .enter()
          .append('circle')
          .attr('id', function(d) {
            return 'city_' + surveyDataByCity.get(d.name)._id
          })
          .attr('class', 'q_circle')
          .attr('cx', function(d) {
            return d.x;
          })
          .attr('cy', function(d) {
            return d.y;
          })
          .attr('r', function(d) {
            return d.r;
          }) /// size smallest circle for all !!
          .attr('fill', function(d) {
            return circleColour(surveyDataByCity.get(d.name))
          })
          .style('opacity', 0)
          .on("mouseover", function(d) {
            mouseover(surveyDataByCity.get(d.name))
          })
          .on("mouseout", mouseout)
          .on("click", function(d) {
            selectCity(surveyDataByCity.get(d.name))
          })
          .transition()
          .delay(function(d, i) {
            return i * delayQCircles
          })
          .style('opacity', function(d) {
            if (citySelectedId) {
              var id = surveyDataByCity.get(d.name)._id;
              return (id == citySelectedId) ? 1 : opacityUnselected;
            } else return 1;
          })
      })
      .attr('transform', function(d) {
        var xTrans = (xRange.length > 1) ? (d.columnNr * distanceHor - d._side / 2 + paddingLeft) : distanceHor - d._side / 2 + 10;
        var yTrans = (d.rowNr * distanceVer - d._side / 2 + paddingTop - 30);
        return 'translate(' + xTrans + ',' + yTrans + ')'
      })

    // adding amount
    qCircles.selectAll('.rate-groups')
      .append('text')
      .attr('class', 'amount')
      .attr('dx', function(d) {
        return d._side
      })
      .attr('dy', 10)
      .text(function(d) {
        return d.values.length
      })
      /*.attr('opacity',0)
      .transition()
      .delay(function(d, i){ return i * delayAmount })
      .attr('opacity',1)*/

    qCircles.selectAll('.q_circle').attr('r', min)
    qCircles.selectAll('#city_' + citySelectedId).classed('selected', true)
    highlightClonedCircles();
  }

  /* ----- matrix single functions -----*/

  function showMatrixSingle(q, subtype) {
    activeQuestion = q;
    if (qCircles) qCircles.transition().duration(removeDuration).style('opacity', 0).remove()

    var xdomain = [];
    var ydomain = [];

    var marginLeft = 40,
      marginRight = 90; // to do: different values according to the question?

    var qRaw = q.substring(0, 3);
    var allTitles = titlesByCode.get(qRaw).answers;

    if (questionGroups[q]) var rowTitles = allTitles.filter(function(d) {
      return d.type == "row"
    }).sort(function(a, b) {
      return a.position - b.position
    }).slice(questionGroups[q][0], questionGroups[q][1]);
    else var rowTitles = allTitles.filter(function(d) {
      return d.type == "row"
    }).sort(function(a, b) {
      return a.position - b.position
    });
    var colTitles = allTitles.filter(function(d) {
      return d.type == "col"
    }).sort(function(a, b) {
      return a.position - b.position
    });

    var nRow = rowTitles.length;
    var nCol = colTitles.length;

    for (var i = 0; i < nCol; i++) {
      xdomain.push(i + 1)
    }
    for (var i = 0; i < nRow; i++) {
      ydomain.push(i + 1)
    }

    var x = d3.scale.ordinal()
      .domain(xdomain)
      .rangeRoundBands([0, (visWidth - marginLeft - marginRight)]);

    var y = d3.scale.ordinal()
      .domain(ydomain)
      .rangeRoundBands([0, height]);

    positionCirclesMatrixSingle(q, nRow, nCol, x.range(), y.range(), subtype)
    showLabelsMatrixSingle(q, rowTitles, colTitles, nRow, nCol, x.range(), y.range(), subtype)


    cityCircles.style('opacity', function(d) {
        return (d._id == citySelectedId) ? 1 : opacityUnselected
      })
      .transition()
      .ease('cubic')
      .delay(function(d, i) {
        return i * delayCircles
      })
      .attr('cx', function(d) {
        if (d.x[q]) return d.x[q];
        else return -20
      })
      .attr('cy', function(d) {
        if (d.y[q]) return d.y[q];
        else return -20
      })
      .attr('r', qRadiusCircle[q])

    qCircles.selectAll('circle')
      .transition()
      .delay(function(d, i) {
        return i * delayQCircles
      })
      .style('opacity', function(d) {
        if (citySelectedId) {
          var id = jQuery(this).prop('id');
          id = Number(id.slice(5))
          return (id == citySelectedId) ? 1 : opacityUnselected;
        } else return 1;
      })

    qCircles.selectAll('#city_' + citySelectedId).classed('selected', true)

    highlightClonedCircles();

  }

  function showLabelsMatrixSingle(q, rowTitles, colTitles, nRow, nCol, xRange, yRange, subtype) {
    var extraHorSpace = 0,
      titleAlign = 'start',
      extraVertSpace = 0;
    paddingLeft = 125, marginTop = 50;

    if (nRow == 1) var yHeight = 300;
    else var yHeight = (yRange[1] - yRange[0]);

    if (subtype == 'rating') {
      titlesPos = 0;
      extraHorSpace = 35;
      extraVertSpace = 14;
      titleAlign = 'middle'
    } else titlesPos = height - 10;

    if (nRow == 1) titleAlign = 'start';

    // inverting column titles order for q30 and q34
    var qRaw = q.substring(0, 3);
    if (qRaw == 'q34' || qRaw == 'q30') colTitles.reverse(); // to do change titles

    //
    labelsGroup.append('g')
      .selectAll(".label")
      .data(rowTitles)
      .enter()
      .append('text')
      .attr('class', 'label')
      .attr('y', 20)
      .attr("text-anchor", "end")
      .attr("transform", function(d, i) {
        return "translate(" + (paddingLeft - 20) + "," + (yRange[i] + yHeight - marginTop + extraVertSpace) + ")"
      })
      .attr('dx', 0)
      .attr('dy', 0)
      .text(function(d) {
        return d.text
      })
      .call(wrap, (paddingLeft))
      .attr('opacity', 0)
      .transition()
      .delay(function(d, i) {
        return i * delayLabels
      })
      .attr('opacity', 1)

    labelsGroup.append('g')
      .selectAll(".label")
      .data(colTitles)
      .enter()
      .append('text')
      .attr('class', 'label')
      .attr('y', 20)
      .attr("text-anchor", titleAlign)
      .attr("transform", function(d, i) {
        if (nRow == 1) {
          return "translate(" + (xRange[i] + paddingLeft) + ", 315)";
        } else return "translate(" + (xRange[i] + extraHorSpace + paddingLeft) + "," + titlesPos + ")"
      })
      .attr('dx', -5)
      .attr('dy', 0)
      .text(function(d) {
        return d.text.replace(/<br>/g, ' ')
      })
      .call(wrap, 100)

    if (nRow == 1) labelsGroup
      .append('line')
      .attr('class', 'axis-line')
      .attr('x1', paddingLeft - 10)
      .attr('x2', visWidth)
      .attr('y1', 317)
      .attr('y2', 317)

    else labelsGroup.selectAll('line')
      .data(rowTitles)
      .enter()
      .append('line')
      .attr('class', 'axis-line')
      .attr('x1', paddingLeft - 10)
      .attr('x2', visWidth - 30)
      .attr('y1', function(d, i) {
        return yRange[i] + yHeight - 5 + extraVertSpace;
      })
      .attr('y2', function(d, i) {
        return yRange[i] + yHeight - 5 + extraVertSpace;
      })
  }

  function positionCirclesMatrixSingle(q, nRow, nCol, xRange, yRange, subtype) {
    labelsGroup.selectAll("*").style('opacity', 0).remove()

    var countArray = []; //dots for each bar
    var resultValue;
    var paddingHor = 10,
      paddingVer = 30,
      marginLeft = 135,
      marginTop = 10,
      extraVertSpace = 0; //paddingVer includes space for amount
    var circlePad = 1;
    var cityArray = []; //cities which have already coordinates
    if (subtype == 'rating') {
      extraVertSpace = 14;
    }

    for (var i = 0; i < nCol; i++) countArray[i] = 0;

    var circlePerRow = 3;
    var qRaw = q.substring(0, 3);

    qCircles = svg.append('g')
      .attr('class', q)
      .attr('transform', 'translate(' + dotsTranslationValue + ',10)')

    if (nRow > 1) {
      var max = 0
        // detecting max number of cities for one group
      for (var i = 0; i < nRow; i++) {
        if (questionGroups[q]) qValue = qRaw + alpha[i + questionGroups[q][0]]
        else qValue = q + alpha[i];
        surveyData.forEach(function(d) {
          cityResults = surveyDataByCity.get(d.q01x);
          var resultValue = cityResults[qValue];
          if (resultValue) countArray[resultValue - 1] += 1;
        })
        var maxArray = Math.max.apply(null, countArray);
        if (maxArray > max) max = maxArray;
        for (var j = 0; j < nCol; j++) countArray[j] = 0;
      }

      // calculating width and height in which the circles must fit

      var xWidth = (xRange[1] - xRange[0]) - paddingHor;
      var yHeight = (yRange[1] - yRange[0]) - paddingVer;

      var nw = Math.round(Math.sqrt(max * xWidth / yHeight))
      qRadiusCircle[q] = Math.floor((xWidth / nw - 1) / 2);
      if (qRadiusCircle[q] > 5) qRadiusCircle[q] = 5;
      circlePerRow = nw;

      var nh = nw * yHeight / xWidth;
    } else {
      qRadiusCircle[qRaw] = radiusCircle;
    }

    for (var i = 0; i < nRow; i++) {
      if (questionGroups[q]) qValue = qRaw + alpha[i + questionGroups[q][0]]
      else qValue = q + alpha[i];
      if (nRow == 1) yPos = 300;
      else yPos = yRange[i] + yHeight + marginTop + extraVertSpace;
      for (var j = 0; j < nCol; j++) countArray[j] = 0;
      surveyData.forEach(function(d) {
        var cloneCount = 0;
        cityResults = surveyDataByCity.get(d.q01x);
        var resultValue = cityResults[qValue];
        if (resultValue) {
          if (qRaw == "q30") resultValue = 5 - Number(resultValue); // this question has a position 0 for N/A and is presented inverted
          else if (qRaw == "q34") resultValue = 6 - Number(resultValue); // this question is presented inverted
          if (cityArray.indexOf(d.q01x) < 0) {
            d.x[q] = (countArray[resultValue - 1] % circlePerRow) * (qRadiusCircle[q] * 2 + circlePad) + xRange[resultValue - 1] + marginLeft;
            d.y[q] = yPos - Math.floor(countArray[resultValue - 1] / circlePerRow) * (qRadiusCircle[q] * 2 + circlePad);
            cityArray.push(d.q01x);
          } else {
            var newCircle = qCircles.append('circle')
              .attr('class', 'q_circle')
              .attr('id', 'city_' + d._id)
              .attr('r', qRadiusCircle[q])
              .attr('fill', circleColour(d))
              .on("mouseover", function() {
                mouseover(d)
              })
              .on("mouseout", mouseout)
              .on("click", function() {
                selectCity(d)
              })
              .attr('cx', (countArray[resultValue - 1] % circlePerRow) * (qRadiusCircle[q] * 2 + circlePad) + xRange[resultValue - 1] + marginLeft)
              .attr('cy', yPos - Math.floor(countArray[resultValue - 1] / circlePerRow) * (qRadiusCircle[q] * 2 + circlePad))
              .style('opacity', 0)
          }
          countArray[resultValue - 1] += 1;
        }
      })

      labelsGroup.append('g').selectAll('.amount')
        .data(countArray)
        .enter()
        .append('text')
        .attr('class', 'amount')
        .attr('y', function(d) {
          return yPos - Math.ceil(d / circlePerRow) * (qRadiusCircle[q] * 2 + circlePad) + 7
        })
        .attr('x', function(d, i) {
          return xRange[i] - 16 + marginLeft
        })
        .text(function(d) {
          return d
        })
    }

    //check if all cities have coordinates otherwise assign negative coords so that they are not visible
    cityCoordsCheck(q);


  }

  function cityCoordsCheck(q) {
    surveyData.forEach(function(d) {
      if (!d.x[q]) d.x[q] = getRandomArbitrary(0, visWidth), d.y[q] = -50;
    })

  }

  function getRandomArbitrary(min, max) {
    return parseInt(Math.random() * (max - min) + min);
  }

  function circleColour(d) { // to do test when having values
    if (activeColour == 'pop') return populationQuantile(Number(d.q03a));
    else if (activeColour == 'reg') return continentColors[d['UN Region Level 2']].color
    else if (activeColour == 'gdp') {
      var gdp = d['GDP/capita ($PPP)'].replace(',', '');
      return wealthQuantile(gdp);
    }
  }

  function circleColourQ(d) {
    if (activeColour == 'pop') {
      return populationQuantile(Number(surveyDataByCity.get(d.q01x).q03a));
    } else if (activeColour == 'reg') return continentColors[surveyDataByCity.get(d.q01x)['UN Region Level 2']].color
    else if (activeColour == 'gdp') return wealthQuantile(surveyDataByCity.get(d.q01x)['GDP/capita ($PPP)'].replace(',', ''))
  }

  function colourBy(d) {
    activeColour = d.id;
    jQuery('.selected-item').text(d.name[current_language]);
    jQuery('#subitem').slideUp()
    d3.selectAll('.citycircle')
      .attr('fill', function(g) {
        return circleColour(g)
      })

    surveyData.forEach(function(d) {
        circleColourQ(d)
        if (qCircles) qCircles.selectAll('#city_' + d._id).attr('fill', circleColourQ(d))
      })
      //legend
    legenddiv.selectAll('.legend').style('display', 'none');
    legenddiv.select('.' + d.id + '-legend').style('display', 'block')
  }

  function mouseover(d) {
    d3.selectAll('#city_' + d._id).classed('mouseover', true)
      //var textTooltip = '<div class="title">'+d.q01x+', '+d.q01+'</div>' + language_data.strings[current_language].population + ': <strong>'+ numberWithCommas(d.q03a)+'</strong>'; //to do change back
    var textTooltip = '<div class="title">' + d.q01x + ', ' + d.q01 + '</div>' + language_data.strings[current_language].population + ': <strong>' + d.q03a + '</strong>';
    tooltipdiv.html(textTooltip)
      .style("top", d3.event.pageY - 40 + "px")
      .style("left", d3.event.pageX + 25 + "px")
    jQuery('.arrow_box').css('display', 'block');
  }

  function mouseout() {
    d3.selectAll('.mouseover').classed('mouseover', false)
    jQuery('.arrow_box').css('display', 'none');
  }

  function selectCity(d) {
    citySelected = d.q01x;
    citySelectedId = d._id;
    opacityUnselected = 0.7;
    showCityInfo();
    d3.selectAll('.selected').classed('selected', false);
    d3.selectAll('.q_circle').style('opacity', opacityUnselected);
    d3.selectAll('.citycircle').style('opacity', function() {
        return (jQuery(".rate-groups")[0]) ? 0 : opacityUnselected
      }) //
    if (Object.keys(questionGroups).indexOf(activeQuestion) < 0) d3.selectAll('#city_' + d._id).style('opacity', 1).classed('selected', true) //only if it's not a matrix multi/menu
    else {
      d3.selectAll('#city_' + d._id).classed('selected', true)
      qCircles.selectAll('#city_' + d._id).style('opacity', 1)
    }

    gBorders.select('.mapcircle').remove();
    gBorders.append('circle').attr('class', 'mapcircle')
      .attr('cx', projection([d.POINT_X, d.POINT_Y])[0])
      .attr('cy', projection([d.POINT_X, d.POINT_Y])[1])
      .attr('r', 5)
      .attr('fill', "#D1CBC6")
  }

  function showCityInfo() {
    cityInfo.style('display', 'block')
    cityData.selectAll('div').remove()
    var dataCity = surveyDataByCity.get(citySelected)
    cityInfo.select('.city-name').text(dataCity.q01x + ', ' + dataCity.q01)

    cityData.append('div').attr('class', 'title').text(language_data.strings[current_language].population)
    cityData.append('div').text(numberWithCommas(dataCity.q03a) + ' (' + dataCity.q03b + ')')

    cityData.append('div').attr('class', 'title').text(language_data.strings[current_language].people_working_for_the_city_local_government)
    cityData.append('div').text(dataCity.q07)

    cityData.append('div').attr('class', 'title').text(language_data.strings[current_language].expenditure)
    cityData.append('div').text(numberWithCommasDolarFormat(dataCity.q18b));

    cityData.append('div').attr('class', 'title').text(language_data.strings[current_language].revenue)
    cityData.append('div').text(numberWithCommasDolarFormat(dataCity.q17b));

    cityData.append('div').attr('class', 'button red-button').html('<a href= "' + language_data.profile_page_path[current_language] + '/?city=' + dataCity['city id'] + '">' + language_data.profile[current_language] + '</a>')

  }

  function highlightCity(id) {
    d3.selectAll('#city_' + id).classed('highlighted-' + highlightIndex, true)
    highlightIndexId[id] = highlightIndex; // highlightIndex used to assign class
    if (highlightIndex < 3) highlightIndex += 1;
    else highlightIndex = 0;
    highlightCount++;
  }

  function unHighlight(id) {
    d3.selectAll('#city_' + id).classed('highlighted-' + highlightIndexId[id], false)
    if (highlightIndex > 0) {
      highlightIndex -= 1;
    } else highlightIndex = 3;
    delete highlightIndexId[id];
    highlightCount--;
  }

  function highlightClonedCircles() {
    for (var id in highlightIndexId) {
      d3.selectAll('#city_' + id).classed('highlighted-' + highlightIndexId[id], true)
    }
  }

  function wrap(text, width) {
    text.each(function() {
      var text = d3.select(this),
        words = text.text().split(/\s+/).reverse(), //.split(/\s|-/).reverse()
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1, // ems
        y = text.attr("y"),
        dy = parseFloat(text.attr("dy")),
        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");

      if (words.length > 1) {
        while (word = words.pop()) {
          line.push(word);
          tspan.text(line.join(" "));
          if (tspan.node().getComputedTextLength() > width) {
            line.pop();
            tspan.text(line.join(" "));
            line = [word];
            tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em")
              .attr('class', 'line-' + lineNumber).text(word);
          }
        }
      }
      // no wrapping in case the text has only one word even when it's too long
      else tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", "0em").text(words.pop());
    });
  }

  jQuery('#scroll').click(function() {
    jQuery('html,body').animate({
      scrollTop: jQuery("#section" + questions[0]).offset().top - questionsHeight
    }, 1000);
  })


  jQuery("#city-search").tokenInput(allCityNames, {
    placeholder: language_data.strings[current_language].highlight_cities,
    //resultsLimit:4,
    onAdd: function(item) {
      highlightCity(item.id);
    },
    onDelete: function(item) {
      unHighlight(item.id);
    }
  });
  jQuery("#token-input-city-search").css('width', '150px'); // workaround to make placehoder visible

  jQuery('#close').click(function() {
    opacityUnselected = 1;
    citySelectedId = null;
    gBorders.select('.mapcircle').remove()
    jQuery('#city-info').fadeOut();
    d3.selectAll('.q_circle').style('opacity', opacityUnselected);
    d3.selectAll('.citycircle').style('opacity', function() {
      return (jQuery(".rate-groups")[0]) ? 0 : opacityUnselected
    }); // only for groupsmatrix it will hide the city circles
    d3.selectAll('.selected').classed('selected', false)
  });

  jQuery('#subitem').hide();

  jQuery('#mainitem').click(function() {
    jQuery('#subitem').slideToggle()
  })

  jQuery('#legendclose').click(function() {
    jQuery(this).text(function(i, text) {
      return text === language_data.strings[current_language].hide_legend ? language_data.strings[current_language].show_legend : language_data.strings[current_language].hide_legend;
    })
    jQuery('.legendtoggle').slideToggle()
  })

  function numberWithCommasDolarFormat(x) {
    if (x > 0) return '$' + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    else return language_data.strings[current_language].no_answer;
  }

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  /**
   * activate -
   *
   * @param index - index of the activated section
   */
  chart.activate = function(index) {
    activeIndex = index;
    var sign = (activeIndex - lastIndex) < 0 ? -1 : 1;
    var scrolledSections = d3.range(lastIndex + sign, activeIndex + sign, sign);
    scrolledSections.forEach(function(i) {
      activateFunctions[i]();
    });
    lastIndex = activeIndex;

    if (activeQuestion == 'q00') var title = '';
    else var title = jQuery.grep(shortQuestions, function(e) {
      return e.qcode == activeQuestion;
    })[0]['theme_' + current_language];
    d3.select('.theme-title').text(title);
  };

  /**
   * update
   *
   * @param index
   * @param progress
   */
  chart.update = function(index, progress) {
    updateFunctions[index](progress);
  };

  // return chart function
  return chart;
}

d3.selection.prototype.size = function() {
  var n = 0;
  this.each(function() {
    ++n;
  });
  return n;
};

function display(error, borders, surveyData, titlesData, shortQuestions) {
  // create a new plot and
  // display it
  var breakpoint = 992; //used for responsiveness
  var width = jQuery('#vis').width();
  var headerHeight = (width >= breakpoint) ? 84 : 300;
  var rawHeight = jQuery(document).height() - headerHeight; //without website header
  if (rawHeight > 1000) rawHeight = 1000;
  var questionsHeight = (width >= breakpoint) ? rawHeight / 2 + 180 : 270; //top position of questions
  var overlayHeight = (width >= breakpoint) ? questionsHeight - 160 : 670

  // get current page language from the lang attribute of the html
  // element (this is set by the polylang WP plugin); default is en-GB
  var current_language = jQuery('html').attr('lang') || 'en-GB';
  var plot = scrollVis(current_language, borders, surveyData, titlesData, shortQuestions, rawHeight, questionsHeight)

  d3.select("#vis")
    .call(plot)

  // setup scroll functionality
  var scroll = scroller()
    .container(d3.select('#questions'));

  // pass in .step selection as the steps
  scroll(d3.selectAll('.step'));

  // reposition top questions, overlay, filling nr of cities

  d3.select('#questions').style('top', questionsHeight + 'px'); //
  d3.select('.overlay').style('top', overlayHeight + 'px');
  d3.select('#cities-count').text(surveyData.length)

  // setup event handling
  scroll.on('active', function(index) {
    // highlight current step text
    if (width >= breakpoint) {
      d3.selectAll('.step')
        .transition()
        .duration(100)
        .style('opacity', function(d, i) {
          if (i == index) return 1;
          else if ((i == index + 1 || i == index - 1)) return 0.3;
          else return 0;
        })
        .style('font-weight', function(d, i) {
          if (i == index) return '900';
          else return 'normal';
        });
    } else {
      d3.selectAll('.step')
        .transition()
        .duration(100)
        .style('opacity', function(d, i) {
          if (i == index) return 1;
          else return 0;
        })
    }

    // activate current section
    plot.activate(index);
  });

  scroll.on('progress', function(index, progress) {
    plot.update(index, progress);
  });

}


function loadSurvey() {
  var width = jQuery('#vis').width();
  var breakpoint = 992; //used for responsiveness
  jQuery(document).ready(function() {
    jQuery("#sticker2").sticky({
      topSpacing: 82
    });
  });
  jQuery(document).scroll(function() {
    if (jQuery(this).scrollTop()) {
      jQuery('.surveyvis-header').fadeIn(400);
      jQuery('.legenddiv').fadeIn(800);
      jQuery('.overlay').fadeOut(400);
    } else {
      jQuery('.overlay').fadeIn(800);
      jQuery('.legenddiv').fadeOut(400);
      if (width > breakpoint) {
        jQuery('.surveyvis-header').fadeOut(600);
      }
    }
  });

  jQuery(window).bind('resize', function(e) {
    if (window.RT) clearTimeout(window.RT);
    window.RT = setTimeout(function() {
      this.location.reload(false); /* false to get page from cache */
    }, 100);
  });

  var url = '/app/themes/urbangovernance/';
  //var url ='';
  var current_language = jQuery('html').attr('lang') || 'en-GB';
	var data_package_id = getParameterByName('dp_id');
	var data_package_environment = getParameterByName('dp_env');
	var api_query;

  /**
   * if no dp_env or dp_id was provided as GET URI parameter, we first
   * try to load a JSON configuration from the dp_env=production API
   * endpoint; if this is valid JSON we then try to load the actual
   * app data and metadata (world_map, survey_results_data, titles_data,
   * short_question_data) from this configuration; if no valid JSON is
   * returned, we fall back to loading data distributed with the theme.
   * @x-technical-debt: rather than just checking that a valid JSON file is
   * returned, we should check that the JSON document validates against our
   * own schema (which doesn't exist yet) and that each resource listed in the
   * JSON document actually exists, otherwise falling back to the default.
   */
	if(data_package_id && data_package_id.length > 0) {
		api_query = '/api/v0/data-package/by-id/' + data_package_id;
	} else if (data_package_environment && data_package_environment.length > 0 ){
		api_query = '/api/v0/data-package/by-environment/' + data_package_environment;
	} else {
    api_query = '/api/v0/data-package/by-environment/production';
  }

	d3.json(api_query, function(error, data) {
		if(!error) {
      queue()
  			.defer(d3.json, data.world_map)
  			.defer(d3.csv, data.survey_results_data)
  			.defer(d3.json, data.titles_data)
  			.defer(d3.csv, data.short_question_data)
  			.await(display);
    } else {
      queue()
        .defer(d3.json, url + "data/world-countries_noantarctica.json")
        .defer(d3.csv, url + "data/SurveyResults_Numericaldata.csv")
        .defer(d3.json, url + "data/titles.json")
        .defer(d3.csv, url + "data/themes-surveyquestions-short.csv")
        .await(display);
    }
	});
}

//module.exports = loadSurvey;

/**
 * @x-technical-debt: add credits
 * @x-technical-debt: refactor to module and bundle once with all scripts
 */
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
