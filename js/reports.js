var displayReport = function(current_language, activeCity, error, borders, surveyData, titlesDataFull) {
  //console.log('url=',location.search, window.location.search.substring(1))
  var width = jQuery('#map').width();
  var rawHeight = 320;
  var margin = {
    top: 10,
    left: 0,
    bottom: 10,
    right: 0
  };
  var height = rawHeight - margin.top - margin.bottom;
  var scale = width * 140 / 878;
  var titlesData = titlesDataFull[current_language];

  var alpha = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']; //for matrix

  cityData = d3.select('.city-data')

  var citiesArray = [];
  surveyData.forEach(function(d) {
    citiesArray.push(d.q01x);
  })

  // showing map with city ------
  var projection = d3.geo.equirectangular()
    .scale(scale)
    .rotate([-10, 0])
    .translate([width / 2, rawHeight / 2 + 20])
    .precision(.1);

  var path = d3.geo.path()
    .projection(projection);

  var svg = d3.select("#map").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", rawHeight)
    .append('g')
    .attr('transform', "translate(" + margin.left + ',' + margin.top + ")")
    .style("display", "block")
    .style("margin", "auto")

  var gBorders = svg.append("g").attr('class', 'borders')
  var g = svg.append("g").attr('class', 'dots')

  var borders = gBorders.selectAll(".borders")
    .data(borders.features)
    .enter()
    .append("path")
    .attr("class", "borders")
    .attr("d", path)

  var cityCircle = gBorders.append('circle').attr('class', 'mapcircle')
    .attr('r', 5)
    .attr('fill', "#FF0009")

  // showing questions/
  titlesData.sort(function(a, b) {
    return a.position - b.position
  })

  var titles = []; //array with all questions

  titlesData.forEach(function(d) {
    d.questions.forEach(function(g) {
      if (g.qcode) {
        titles.push(g);
      }
    })
  })

  var questionRows = d3.select('.questions').selectAll('.theme-row')
    .data(titlesData)
    .enter()
    .append('div')
    .attr('class', 'row theme-row')
    .html(function(d) {
      return '<h4>' + d.heading + '</h4>'
    })
    .each(function(d) {
      d.questions.sort(function(a, b) {
        return a.position - b.position
      })
      var questions = d3.select(this).selectAll('.question-row')
        .data(d.questions)
        .enter()
        .append('div')
        .attr('class', 'row question-row')
        .attr('id', function(g) {
          return g.qcode;
        })
      questions.append('div')
        .attr('class', 'col-md-12 question')
        .html(function(g) {
          return g.heading.replace(/<br>/g, ' ');
        })

      questions.append('div')
        .attr('class', 'col-md-12 unavailable')
        .html(language_data.strings[current_language].no_answer)

    })

  var surveyDataCity;

  displayCityAnswers(null, activeCity)

  function displayCityAnswers(ev, selectedCity) {

    surveyDataCity = jQuery.grep(surveyData, function(e) {
      return e['city id'] == selectedCity;
    })[0];

    cityCircle.attr('cx', projection([surveyDataCity.POINT_X, surveyDataCity.POINT_Y])[0])
      .attr('cy', projection([surveyDataCity.POINT_X, surveyDataCity.POINT_Y])[1])

    // showing city info -----

    d3.select('.city-name').html('<h3>' + surveyDataCity.q01x + ', <span style="text-transform:uppercase">' + surveyDataCity.q01 + '</span></h3>')

    cityData.selectAll('div').remove();
    // city / country
    cityData.append('div').html('<h4>' + surveyDataCity.q01x + ', <span style="text-transform:uppercase">' + surveyDataCity.q01 + '</span></h4>')

    // metropolitan area
    if (surveyDataCity.q04x) {
      cityData.append('div').attr('class', 'title').html(language_data.strings[current_language].metropolitan_area)
      cityData.append('div').text(surveyDataCity.q04x);
    }
    // world region
    cityData.append('div').attr('class', 'title').html(language_data.strings[current_language].world_region)
    cityData.append('div').text(surveyDataCity['UN Region Level 2']);

    // population
    cityData.append('div').attr('class', 'title').html(language_data.strings[current_language].population)
    cityData.append('div').text(surveyDataCity.q03a + ' (' + surveyDataCity.q03b + ')')

    // people working
    cityData.append('div').attr('class', 'title').html(language_data.strings[current_language].people_working_for_the_city_local_government)
    cityData.append('div').text(surveyDataCity.q07);

    // governance challenges
    cityData.append('div').attr('class', 'title').html(language_data.strings[current_language].governance_challenges)
    cityData.append('div').attr('class', 'governance-challenges')


    // print button
    cityData.append('div').attr('class', 'button red-button').attr('id', 'print-page').html(language_data.strings[current_language].print_profile).on('click', function() {
      window.print()
    });

    questionRows.selectAll('.answer').remove();
    var charttypes = [];
    d3.selectAll('.question-row')
      .append('div')
      .attr('class', function(d) {
        if (d.type.family == "matrix" && d.qcode == 'q13') return 'col-md-10 answer';
        else if (d.type.family == "open_ended" || d.type.family == "multiple_choice" || d.type.family == "single_choice" || d.qcode == "q36") return 'col-sm-12 col-md-8 answer';
        else return 'col-md-12 answer'
      })

    .each(function(d) {
      var q = d.qcode;
      var chart = d.type.family;
      var subtype = d.type.subtype;
      charttypes.push([q, chart, subtype]);

      if (chart == "single_choice") {
        var html = showSingleChoiceQuestion(q, d.answers);
        d3.select(this).html(html);
      } else if (chart == "multiple_choice") {
        var html = showMultipleChoiceQuestion(q, d.answers);
        d3.select(this).html(html);
      } else if (chart == "open_ended") {
        var html = showOpenEndedQuestion(q, subtype, d.answers);
        d3.select(this).html(html);
      } else if (chart == "matrix") {
        if (subtype == "single" || subtype == "rating") {
          if (q == "q28") var html = showMatrixSingleTextQuestion(q, d.answers);
          else var html = showMatrixSingleChartQuestion(q, d.answers, subtype);
          d3.select(this).html(html);
        } else if (subtype == "multi") {
          var html = showGroupsMatrixMultiQuestion(q, d.answers);
          d3.select(this).html(html);
        } else {
          if (subtype == 'menu' && q != 'q13' && q != 'q36') showGroupsMatrixMenuQuestion(q, d.answers);
          else {
            var html = showGroupsMatrixMenuQuestionAsText(q, d.answers);
            d3.select(this).html(html);
          }
        }
      }
    })
  }

  function showSingleChoiceQuestion(q, answersArray) {
    var value = jQuery.grep(answersArray, function(e) {
      return e.position == surveyDataCity[q];
    })[0];
    var answer = '';

    if (value) {
      answer += '<strong>' + value.text + '</strong>';
    } else answer += language_data.strings[current_language].no_answer;

    if (surveyDataCity[q + 'x']) {
      var other = jQuery.grep(answersArray, function(e) {
        return e.type == "other";
      })[0];

      if (other) answer += '<br>' + other.text + ': <strong>' + surveyDataCity[q + 'x'] + '</strong>';
      else answer += '<br><strong>' + surveyDataCity[q + 'x'] + '</strong>';
    }
    return answer;
  }

  function showMultipleChoiceQuestion(q, answersArray) {
    var rows = jQuery.grep(answersArray, function(e) {
      return e.type == "row";
    });

    var answer = '<table class="table table-hover table-condensed">';
    var challenges = '<ul>';

    var noAnswer = true;

    rows.forEach(function(d, i) {
      var qValue = q + alpha[i];
      if (surveyDataCity[qValue]) {
        noAnswer = false;
        answer += '<tr><td class="col-md-7">' + d.text + '</td><td class="col-md-1"><span class="glyphicon glyphicon-ok" style="color:#FF0009"></span></td></tr>';
        if (q == 'q33') challenges += '<li>' + d.text + '</li>';
      } else {
        answer += '<tr><td class="col-md-7">' + d.text + '</td><td class="col-md-1"></td></tr>';
      }
    })
    answer += '</table>';

    if (surveyDataCity[q + 'x']) {
      var other = jQuery.grep(answersArray, function(e) {
        return e.type == "other";
      })[0];
      answer += '<br>' + other.text + ': <strong>' + surveyDataCity[q + 'x'] + '</strong>';
      if (q == 'q33') challenges += '<li>' + surveyDataCity[q + 'x'] + '</li>';
    }

    challenges += '</ul>';
    if (q == 'q33') d3.select('.governance-challenges').html(challenges); //for info panel

    if (noAnswer) {
      jQuery('#' + q + ' .unavailable').show();
      jQuery('#' + q + ' .answer').hide()
    }
    return answer;
  }

  function showMatrixSingleTextQuestion(q, answersArray) {

    var answer = '';
    var rows = jQuery.grep(answersArray, function(e) {
      return e.type == "row";
    });
    var noAnswer = true;

    rows.sort(function(a, b) {
      return a.position - b.position
    });

    answer += '<table class="table table-hover table-condensed">'

    rows.forEach(function(d, i) {
      var qValue = q + alpha[d.position - 1];

      if (surveyDataCity[qValue]) {
        var answerText = jQuery.grep(answersArray, function(e) {
          return (e.position == surveyDataCity[qValue]) && (e.type == "col");
        })[0].text;
        noAnswer = false
      } else answerText = "n/a";
      answer += '<tr><td>' + d.text + "</td><td><strong>" + answerText.replace(/<br>/g, ' ') + "</strong></td></tr>";
    })
    answer += '</table>';

    if (surveyDataCity[q + 'x']) {
      var other = jQuery.grep(answersArray, function(e) {
        return (e.type == "other");
      })[0];
      answer += '<br>' + other.text + ': <strong>' + surveyDataCity[q + 'x'] + '</strong>';
    }

    if (noAnswer) {
      jQuery('#' + q + ' .unavailable').show();
      jQuery('#' + q + ' .answer').hide()
    }
    return answer;
  }

  function showMatrixSingleChartQuestion(q, answersArray, subtype) {
    var answer = '';
    var styleValue = (q == 'q30' || q == 'q34') ? 'col-md-1' : 'single-column';
    var styleValueTitle = (q == 'q30' || q == 'q34') ? 'col-md-2' : 'single-title';
    var allTitles = answersArray;
    var noAnswer = true;

    var qRaw = q.substring(0, 3);

    var rowTitles = allTitles.filter(function(d) {
      return d.type == "row"
    }).sort(function(a, b) {
      return a.position - b.position
    });
    var colTitles = allTitles.filter(function(d) {
      return d.type == "col"
    }).sort(function(a, b) {
      return a.position - b.position
    });

    if (q == 'q30') colTitles.shift();

    var nRow = rowTitles.length;
    var nCol = colTitles.length;

    answer += '<table class="table table-hover table-condensed"><thead><tr><th class="' + styleValueTitle + '"></th>'

    colTitles.forEach(function(d) {
      answer += '<th class="' + styleValue + '">' + d.text + '</th>';
    })
    answer += '</tr></thead>';

    for (var j = 1; j <= nRow; j++) {
      answer += '<tr><td class="row-title single-title">' + rowTitles[j - 1].text + '</td>';
      var row = j.toString();
      if (row.length < 2) row = '0' + row;
      var qValue = q + alpha[j - 1];

      for (var i = 0; i < nCol; i++) {
        if (Number(surveyDataCity[qValue]) == 12) {
          if (i == 11) answer += '<td class="col-md-1 rating"><div class="red-block"></div></td>';
          else answer += '<td class="' + styleValue + '"></td>';
        } else {
          if (Number(surveyDataCity[qValue]) > i) {
            answer += '<td class="' + styleValue + ' rating"><div class="red-block"></div></td>';
            noAnswer = false;
          } else answer += '<td class="' + styleValue + '"></td>';
        }
      }
      answer += '</tr>';
    }
    answer += '</table>';

    if (surveyDataCity[q + 'x']) {
      var other = jQuery.grep(answersArray, function(e) {
        return e.type == "other";
      })[0];
      //console.log('other', other, q, answersArray)
      answer += '<br>' + other.text + ': <strong>' + surveyDataCity[q + 'x'] + '</strong>';
    }

    if (noAnswer) {
      jQuery('#' + q + ' .unavailable').show();
      jQuery('#' + q + ' .answer').hide()
    }

    return answer;
  }


  function showGroupsMatrixMultiQuestion(q, answersArray) {
    var answer = '';
    var noAnswer = true;
    var cols = jQuery.grep(answersArray, function(e) {
      return e.type == "col";
    });
    cols.forEach(function(d, i) {
      answer += d.text + ": <strong>";
      // retrieve answers
      answersArray.forEach(function(g) {
        if (g.position) {
          var nr = (g.position).toString(); //row position must match column name
          if (nr.length < 2) nr = '0' + nr;
          qValue = q + nr;

          if (g.type == "row") {
            //console.log('qValue+alpha[d.position-1]',qValue+alpha[d.position-1])
            if (surveyDataCity[qValue + alpha[d.position - 1]] == 1) {
              answerText = g.text;
              if (g.text) noAnswer = false;
              answer += answerText + ", ";
            }
          }
        }
      })
      answer += '</strong><br>';
    });
    if (surveyDataCity[q + 'x']) {
      noAnswer = false;
      var other = jQuery.grep(answersArray, function(e) {
        return e.type == "other";
      })[0];
      answer += '<br>' + other.text + ': <strong>' + surveyDataCity[q + 'x'] + '</strong>';
    }
    if (noAnswer) {
      jQuery('#' + q + ' .unavailable').show();
      jQuery('#' + q + ' .answer').hide()
    }

    return answer;
  }

  function showOpenEndedQuestion(q, subtype, answersArray) {
    //console.log('q',q, 'subtype',subtype)
    var noAnswer = true;

    if (subtype == 'multi') {
      var answer = '<table class="table table-hover table-condensed">';
      answersArray.forEach(function(d, i) {
        var qValue = q + alpha[i];
        answer += '<tr><td>' + d.text + "</td><td><strong>" + surveyDataCity[qValue] + "</strong></td></tr>";
        if (surveyDataCity[qValue]) noAnswer = false;
      })
      answer += '</table>';
    } else {
      var answer = '<strong>' + surveyDataCity[q] + '</strong>';
      if (surveyDataCity[q]) noAnswer = false;
    }

    if (noAnswer) {
      jQuery('#' + q + ' .unavailable').show();
      jQuery('#' + q + ' .answer').hide()
    }

    return answer;

  }

  function showGroupsMatrixMenuQuestion(q, answersArray) {
    var allTitles = answersArray;
    var noAnswer = true;

    var xdomain = [],
      ydomain = [];
    var qRaw = q.substring(0, 3);

    var rowTitles = allTitles.filter(function(d) {
      return d.type == "row"
    }).sort(function(a, b) {
      return a.position - b.position
    });
    var colTitles = allTitles.filter(function(d) {
      return d.type == "col"
    }).sort(function(a, b) {
      return a.position - b.position
    });

    var nRow = rowTitles.length;
    var nCol = colTitles.length;

    var paddingLeft = 250,
      paddingTop = 30;
    var distanceHor = 120,
      distanceVer = 40;
    var rowTitlesWidth = 180;

    var width = nCol * distanceHor + paddingLeft;
    var height = nRow * distanceVer + 2 * paddingTop;

    var svgBase = d3.select("#" + q + ' .answer')
      .append('svg')
      .attr('width', width)
      .attr('height', height)

    var svg = svgBase.append('g')
      .attr('transform', 'translate(0,60)')

    var legendSvg = svgBase.append('g')
      .attr('transform', 'translate(0,10)')
      .attr('class', 'influence-legend')
      .selectAll('.legend-item')
      .data([0, 1, 2, 3])
      .enter()
      .append('g')
      .attr('transform', function(d, i) {
        return 'translate(' + i * 180 + ',10)'
      })
      .attr('class', 'legend-item')

    legendSvg.append('text')
      .text(function(d) {
        return language_data.influence[current_language]['influence_' + d];
      })
      .attr('x', function(d, i) {
        return (i + 1) * 2 + 10
      })

    legendSvg.append('circle')
      .attr('r', function(d, i) {
        return (i + 1) * 2
      })
      .attr('cx', 5)
      .attr('cy', -5) //function(d,i){ return -(i+1)*2 })
      .attr('fill', function(d, i) {
        return (i > 0) ? '#FF0009' : 'none'
      })
      .attr('stroke', function(d, i) {
        return (i == 0) ? '#FF0009' : 'none'
      })

    var labelsGroup = svg.append('g').attr('class', 'labels')
      .attr("transform", "translate(10)")

    var qCircles = svg.append('g')
      .attr('class', q)

    for (var i = 0; i < nCol; i++) {
      xdomain.push(i + 1)
    }
    for (var i = 0; i < nRow; i++) {
      ydomain.push(i + 1)
    }

    var x = d3.scale.ordinal()
      .domain(xdomain)
      .rangeRoundBands([0, (width - 150)], .2);

    var y = d3.scale.ordinal()
      .domain(ydomain)
      .rangeRoundBands([0, (height)], .2);

    // --- labels

    var xRange = x.range();
    var yRange = y.range();

    labelsGroup.append('g').selectAll(".label")
      .data(rowTitles)
      .enter()
      .append('text')
      .attr('class', 'label')
      .attr('y', 20)
      //.attr("text-anchor", "end")
      .attr("transform", function(d, i) {
        return "translate(0" + "," + ((i + 1) * distanceVer + paddingTop - 20) + ")"
      })
      .attr('dx', 0)
      .attr('dy', 0)
      .text(function(d) {
        return d.text
      })
      .call(wrap, rowTitlesWidth)

    labelsGroup.append('g').selectAll(".label")
      .data(colTitles)
      .enter()
      .append('text')
      .attr('class', 'label')
      .attr('y', 20)
      .attr("text-anchor", "middle")
      .attr("transform", function(d, i) {
        return "translate(" + ((i) * distanceHor + paddingLeft) + ", -10)"
      })
      .attr('dx', 0)
      .attr('dy', 0)
      .text(function(d) {
        return d.text.replace(/<br>/g, ' ')
      })
      .call(wrap, distanceHor - 10)

    labelsGroup.selectAll('.xaxis-line')
      .data(rowTitles)
      .enter()
      .append('line')
      .attr('class', 'xaxis-line')
      .attr('x1', rowTitlesWidth + 5)
      .attr('x2', width)
      .attr('y1', function(d, i) {
        return (i + 1) * distanceVer + paddingTop;
      })
      .attr('y2', function(d, i) {
        return (i + 1) * distanceVer + paddingTop;
      })

    var circleDataArray = []

    for (var j = 0; j < nCol; j++) {
      for (var i = 1; i <= nRow; i++) {
        var row = i.toString();
        if (row.length < 2) row = '0' + row;
        var circleData = {};
        var qValue = q + row + alpha[j];
        if (surveyDataCity[qValue]) {
          noAnswer = false;
          circleData.qValue = qValue;
          circleData.value = surveyDataCity[qValue];
          circleData.xPos = j * distanceHor + paddingLeft + 10;
          circleData.yPos = (i + 1) * distanceVer - 10;
          circleDataArray.push(circleData)
        }
      }
    }

    qCircles.selectAll('.qcircle')
      .data(circleDataArray)
      .enter()
      .append('circle')
      .attr('r', function(d) {
        return d.value * 2
      })
      .attr('cx', function(d) {
        return d.xPos
      })
      .attr('cy', function(d) {
        return d.yPos
      })
      .attr('fill', function(d) {
        return (d.value > 1) ? '#FF0009' : 'none'
      })
      .attr('stroke', function(d) {
        return (d.value == 1) ? '#FF0009' : 'none'
      })

    if (surveyDataCity[q + 'x']) {
      var other = jQuery.grep(answersArray, function(e) {
        return (e.type == "other");
      })[0];
      var otherText = '<br>' + other.text + ': <strong>' + surveyDataCity[q + 'x'] + '</strong>';
      d3.select("#" + q + ' .answer').append('div').html(otherText)
    }

    if (noAnswer) {
      jQuery('#' + q + ' .unavailable').show();
      jQuery('#' + q + ' .answer').hide()
    }



  }

  function showGroupsMatrixMenuQuestionAsText(q, answersArray) {
    var noAnswer = true;
    var allTitles = answersArray;

    var qRaw = q.substring(0, 3);

    var rowTitles = allTitles.filter(function(d) {
      return d.type == "row"
    }).sort(function(a, b) {
      return a.position - b.position
    });
    var colTitles = allTitles.filter(function(d) {
      return d.type == "col"
    }).sort(function(a, b) {
      return a.position - b.position
    });

    var nRow = rowTitles.length;
    var nCol = colTitles.length;

    var answer = '';
    if (surveyDataCity[q + 'x']) {
      noAnswer = false;
      answer = '<strong>' + surveyDataCity[q + 'x'] + '</strong><br>';
    }
    answer += '<table class="table table-hover table-condensed"><thead><tr><th></th>';

    colTitles.forEach(function(d) {
      answer += '<th class="matrix-menu">' + d.text + '</th>';
    })

    answer += '</tr></thead>';
    for (var j = 1; j <= nRow; j++) {
      answer += '<tr><td>' + rowTitles[j - 1].text + '</td>';
      for (var i = 0; i < nCol; i++) {
        var row = j.toString();
        if (row.length < 2) row = '0' + row;
        var qValue = q + row + alpha[i];
        if (surveyDataCity[qValue]) {
          noAnswer = false;
          var response = jQuery.grep(colTitles[i].items, function(e) {
            return e.position == +surveyDataCity[qValue];
          })[0];
          //console.log(i, qValue,'--',surveyDataCity[qValue],'--', response)
          var text = response.text;
        } else var text = 'n/a';
        answer += '<td><strong>' + text + '</strong></td>';
      }
      answer += '</tr>';
    }
    return answer + '</table>';

  }
  // other functions ----

  var substringMatcher = function(strs) {
    return function findMatches(q, cb) {
      var matches, substringRegex;

      // an array that will be populated with substring matches
      matches = [];

      // regex used to determine if a string contains the substring `q`
      substrRegex = new RegExp(q, 'i');

      // iterate through the pool of strings and for any string that
      // contains the substring `q`, add it to the `matches` array
      jQuery.each(strs, function(i, str) {
        if (substrRegex.test(str)) {
          matches.push(str);
        }
      });

      cb(matches);
    };
  };

  function wrap(text, width) {
    text.each(function() {
      var text = d3.select(this),
        words = text.text().split(/\s+/).reverse(), //.split(/\s|-/).reverse()
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1, // ems
        y = text.attr("y"),
        dy = parseFloat(text.attr("dy")),
        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");

      if (words.length > 1) {
        while (word = words.pop()) {
          line.push(word);
          tspan.text(line.join(" "));
          if (tspan.node().getComputedTextLength() > width) {
            line.pop();
            tspan.text(line.join(" "));
            line = [word];
            tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em")
              .attr('class', 'line-' + lineNumber).text(word);
          }
        }
      }
      // no wrapping in case the text has only one word even when it's too long
      else tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", "0em").text(words.pop());
    });
  }

  function numberWithCommasDolarFormat(x) {
    return '$' + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
}

d3.selection.prototype.size = function() {
  var n = 0;
  this.each(function() {
    ++n;
  });
  return n;
};

function loadCitiesData() {
  var url = '/app/themes/urbangovernance/';
  var activeCity, current_language;

  current_language = jQuery('html').attr('lang') || 'en-GB';
  activeCity = getParameterByName('city');

  var data_package_id = getParameterByName('dp_id');
  var data_package_environment = getParameterByName('dp_env');
  var api_query;

  if(data_package_id && data_package_id.length > 0) {
    api_query = '/api/v0/data-package/by-id/' + data_package_id;
  } else if (data_package_environment && data_package_environment.length > 0 ){
    api_query = '/api/v0/data-package/by-environment/' + data_package_environment;
  } else {
    api_query = '/api/v0/data-package/by-environment/production';
  }

  d3.json(api_query, function(error, data) {
		if(!error) {
      queue()
  			.defer(d3.json, data.world_map)
  			.defer(d3.csv, data.survey_results_data)
  			.defer(d3.json, data.titles_data)
        .await(displayReport.bind(undefined, current_language, activeCity));
    } else {
      queue()
        .defer(d3.json, url + "data/world-countries_noantarctica.json")
        .defer(d3.csv, url + "data/SurveyResults_Numericaldata.csv")
        .defer(d3.json, url + "data/titles.json")
        .await(displayReport.bind(undefined, current_language, activeCity));
    }
	});
}

//module.exports = loadCitiesData;

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
