<?php
/**
 * Template Name: Data Package (JSON)
 * Description: Page template for JSON description of data packages
 *
 * @package Urban Governance
 * @since Urban Governance 2.1
 */

namespace UrbanGovernanceWPTheme;

$data_package = new DataPackage(pods_v('-2', 'url'), pods_v('last', 'url'));

header('Content-Type: application/json');

if(is_object($data_package)) {
  echo $data_package->to_json();
} else {
  echo json_encode([ 'error' => 'No data package found matching the API query.']);
}
