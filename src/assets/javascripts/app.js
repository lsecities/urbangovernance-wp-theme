'use strict';

global.d3 = require('d3');
global.queue = require('queue');

global.$ = global.jquery = require('jquery');
require('../../../vendor/jquery.tokeninput/jquery.tokeninput.js');
require('../../../vendor/jquery.sticky/jquery.sticky.js');

global.language_data = require('./app/language_data.js');

global.scroller = require('./app/scroller.js');
global.loadMap = require('./app/map.js');
global.loadVis = require('./app/explorevis.js');
global.loadSurvey = require('./app/surveyvis.js');

$(document).ready(function(){
  $("#sticker1").sticky({topSpacing:0});
  $("#sticker2").sticky({topSpacing:46});
});

// depending on visualization required for current page (if any),
// start the appropriate visualization
var visualization_id_functions_map = {
  mapvis: loadMap,
  surveyvis: loadSurvey,
  explorevis: loadVis
}

var visualization_id = $('#vis').data('visualization-id');

if('function' === typeof(visualization_id_functions_map[visualization_id])) {
  visualization_id_functions_map[visualization_id]();
}
