<?php
/**
 * The main template file
 *
 * @package bootstrap-basic
 */

/**
 * Get URI of survey form page for current language
 * This is used to link to it from the 'N Cities currently included box'
 * @x-technical-debt: we are hardcoding the path of the survey form page,
 * whereas this should be configured via the dashboard. But given that this
 * site will likely not change anymore, this is kinda ok.
 */
if(function_exists('pll_get_post')) {
	$target_uri = trailingslashit(get_page_uri(pll_get_post(get_page_by_path('survey-form')->ID)));
} else {
	$target_uri = trailingslashit(get_page_uri(get_page_by_path('survey-form')->ID));
}

get_header();

?>
<div class='fixed-container container-fluid container-large no-events' id="sticker2">
	<div class="row">
		<div class="col-md-12 no-gutter">
			<div id='vis' data-visualization-id='surveyvis'>
	  		</div>
		</div>
	</div>
</div>
<div class='container-fluid container-large no-events'>
	<div class="row"  id='questions'>
		<div id='sections' class="col-xs-7 col-sm-5 col-md-4 col-lg-6">
			<section id="sectionq00" class="step">
				<ol>
					<li>Scroll slowly to see the charts</li>
					<li>Click on a dot for information about a city</li>
					<div id="scroll" class="centered">scroll</div>
				</ol>
			</section>
		</div>
	</div>
	<div class="overlay">
		<div class="top">
			<div class="overlay-big">
				<span id="cities-count"></span> <?php echo pll__('cities')?></div>
				<?php echo pll__('currently included')?>
			</div>
		<div class="bottom">
			<a href="<?php echo $target_uri; ?>"><?php echo pll__('See the results')?></a>
		</div>
	</div>
</div>

<div id="city-info" class="col-md-3">
	<div class="pull-right" id="close">close</div>
	<div class="city-name"></div>
	<div class="city-data"></div>
</div>

	<script>

		loadSurvey();

	</script>
<?php get_footer(); ?>
