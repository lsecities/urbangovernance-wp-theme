<?php
/**
 * Template Name: serviceworker.js
 * Description: Service Worker
 *
 * @package Urban Governance
 * @since Urban Governance 2.1
 */

header('Content-Type: application/javascript');
?>
'use strict';

// Licensed under a CC0 1.0 Universal (CC0 1.0) Public Domain Dedication
// http://creativecommons.org/publicdomain/zero/1.0/

(function() {
    // A cache for core files like CSS and JavaScript
    var staticCacheName = 'static';
    // A cache for pages to store for offline
    var pagesCacheName = 'pages';
    // A cache for images to store for offline
    var imagesCacheName = 'images';
    // Update 'version' if you need to refresh the caches
    var version = 'v1.0.34::';

    // Page to show when offline and user navigates to a page not in cache
    // Make sure the offline_page itself is included in the list of pages
    // to cache below (documentList)
    var offline_page = '/en/offline';

    // List of documents (HTML/JSON/GeoJSON...) to be cached
    var documentList = [
      '/en/offline',
      '/wp-includes/js/jquery/jquery.js?ver=1.12.4',
      '/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1',
      '/app/themes/urbangovernance/js/vendor/respond.min.js',
      '/app/themes/urbangovernance/js/vendor/modernizr.min.js',
      '/app/themes/urbangovernance/js/vendor/html5shiv.js',
      '/app/themes/urbangovernance/js/jquery.sticky.js',
      '/app/themes/urbangovernance/js/vendor/bootstrap.min.js',
      '/app/themes/urbangovernance/js/d3.min.js',
      '/app/themes/urbangovernance/js/language_data.js',
      '/app/themes/urbangovernance/js/jquery.tokeninput.js',
      '/app/themes/urbangovernance/js/scroller.js',
      '/app/themes/urbangovernance/js/queue.js',
      '/wp-includes/js/wp-embed.min.js',
      '/wp-includes/js/wp-emoji-release.min.js',

      '/app/themes/urbangovernance/css/bootstrap-theme.min.css',
      '/app/themes/urbangovernance/css/bootstrap.min.css',
      '/app/themes/urbangovernance/css/token-input.css',
      '/app/themes/urbangovernance/css/font-awesome.min.css',
      '/app/themes/urbangovernance/css/main.css',
      '/app/themes/urbangovernance/style.css',
      '/app/themes/urbangovernance/css/jquery-ui.css',
      '/app/themes/urbangovernance/css/flexvideo.css',

      '/app/themes/urbangovernance/img/logo_lsecities.png',
      '/app/themes/urbangovernance/img/logo_unhabitat.png',
      '/app/themes/urbangovernance/img/logo_uclg_en.png',
      '/app/themes/urbangovernance/img/logo_macarthur.png',
      '/app/themes/urbangovernance/img/ic_scroll_normal.png',
      '/app/themes/urbangovernance/img/ic_detail_close_normal.png',
      '/app/themes/urbangovernance/img/split_button-small_transparent.png',

      '/app/themes/urbangovernance/js/surveyvis.js',
      '/app/themes/urbangovernance/css/surveyvis.css',
      '/app/themes/urbangovernance/js/explorevis.js',
      '/app/themes/urbangovernance/css/explorevis.css',
      '/app/themes/urbangovernance/js/citieslist.js',
      '/app/themes/urbangovernance/css/citieslist.css',
      '/app/themes/urbangovernance/js/reports.js',
      '/app/themes/urbangovernance/css/reports.css',

      // English pages
      '/en/',
      '/en/exploring-the-data/',
      '/en/cities-profiles/',
      '/en/cities-profiles/city-profile/',
      /(\/en\/cities-profiles\/city-profile\/?)/,
      '/en/survey-form/',
      '/en/case-studies/',
      '/en/about/',

      // case studies
      '/en/2016/08/how-governance-gives-shape-to-urban-form/',
      '/es/2016/08/how-governance-gives-shape-to-urban-form-3/',
      '/fr/2016/08/how-governance-gives-shape-to-urban-form-2/',
      '/app/uploads/2016/08/Urban-typology3-362x228.jpg',
      '/app/uploads/2016/08/UA-Where-do-we-live4x11-2-1024x512.jpg',
      '/app/uploads/2016/08/Urban-typology3-2-1024x512.jpg',
      '/app/uploads/2016/08/GovernanceX18-2-1024x512.jpg',
      '/app/uploads/2016/08/UA-Met-boundaries12-2-1024x512.jpg',
      '/app/uploads/2016/08/data-matrix11-2-1024x512.jpg',
      '/app/uploads/2016/08/UA-Met-boundariesx13-2-1024x512.png',

      '/en/2016/05/governance-of-spatial-planning/',
      '/fr/2016/08/governance-of-spatial-planning-2/',
      '/es/2016/08/governance-of-spatial-planning-3/',
      '/app/uploads/2016/05/Screen-Shot-2016-05-18-at-16.47.02-362x228.png',
      '/app/uploads/2016/08/blogspatialplanning-1-1.png',

      '/en/2015/07/influencing-local-policies-2/',
      '/fr/2015/07/influencing-local-policies-3/',
      '/es/2015/07/influencing-local-policies-4/',
      '/app/uploads/2015/05/Casestudy1-362x228.png',
      '/app/uploads/2015/05/Casestudy1-1024x588.png',
      '/app/uploads/2015/05/casestudy12.png',
      '/app/uploads/2015/05/casestudy13.png',
      '/app/uploads/2015/05/casestudy14.png',

      // Spanish pages
      '/es/',
      '/es/explorar-los-datos/',
      '/es/perfiles-de-las-ciudades/',
      '/es/perfiles-de-las-ciudades/informe-de-la-ciudad/',
      /(\/es\/perfiles-de-las-ciudades\/informe-de-la-ciudad\/?)/,
      '/es/formulario-de-la-encuesta/',
      '/es/casos-de-estudio/',
      '/es/acerca-de/',

      // French pages
      '/fr/',
      '/fr/explorer-les-donnees/',
      '/fr/profils-des-villes/',
      '/fr/profils-des-villes/rapport-de-la-ville/',
      /(\/fr\/profils-des-villes\/rapport-de-la-ville\/?)/,
      '/fr/formulaire-denquete/',
      '/fr/etudes-de-cas/',
      '/fr/a-propos-de/',

      // configuration and data for visualizations
      '/api/v0/data-package/by-environment/production',
      '/app/uploads/2016/06/world-countries_noantarctica.json',
      '/app/uploads/2016/09/SurveyResults_Numericaldata_sep2016-final23-1.csv',
      '/app/uploads/2016/06/titles.json',
      '/app/uploads/2016/06/themes-surveyquestions-short.csv'
    ];

    // Store core files in a cache (including a page to display when offline)
    var updateStaticCache = function() {
        return caches.open(version + staticCacheName)
            .then(function (cache) {
                return cache.addAll(documentList.filter(function(item) {
                  return ! (item instanceof RegExp);
                }));
            });
    };

    // Put an item in a specified cache
    var stashInCache = function (cacheName, request, response) {
        caches.open(cacheName)
            .then(function (cache) {
                cache.put(request, response);
            });
    };

    // Limit the number of items in a specified cache.
    var trimCache = function (cacheName, maxItems) {
        caches.open(cacheName)
            .then(function (cache) {
                cache.keys()
                    .then(function (keys) {
                        if (keys.length > maxItems) {
                            cache.delete(keys[0])
                                .then(trimCache(cacheName, maxItems));
                        }
                    });
            });
    };

    // Remove caches whose name is no longer valid
    var clearOldCaches = function() {
        return caches.keys()
            .then(function (keys) {
                return Promise.all(keys
                    .filter(function (key) {
                      return key.indexOf(version) !== 0;
                    })
                    .map(function (key) {
                      return caches.delete(key);
                    })
                );
            })
    };

    self.addEventListener('install', function (event) {
        event.waitUntil(updateStaticCache()
            .then(function () {
                return self.skipWaiting();
            })
        );
    });

    self.addEventListener('activate', function (event) {
        event.waitUntil(clearOldCaches()
            .then(function () {
                return self.clients.claim();
            })
        );
    });

    // See: https://brandonrozek.com/2015/11/limiting-cache-service-workers-revisited/
    self.addEventListener('message', function(event) {
    	if (event.data.command == 'trimCaches') {
    		trimCache(version + pagesCacheName, 35);
    		trimCache(version + imagesCacheName, 20);
    	}
    });

    self.addEventListener('fetch', function (event) {
        var
          request = event.request,
          /**
           * In order to properly cache documents that are requested
           * through HTTP requests with GET query parameters that we
           * don't need, we use the 'A more elegant solution' strategy
           * presented here: http://www.holovaty.com/writing/service-worker-cache-names/
           * Abstracting the code example in this blog post, we
           * list any documents requiring such treatment as RegExp
           * rather than string literals, within the master documentList.
           * RegExp array elements are then filtered out before calling cacheAll,
           * and conversely only RegExp elements are used when fetch()ing here.
           */

        match = documentList
          .filter(function(item) {
            return item instanceof RegExp;
          })
          .map(function(item) {
            return item.exec(request.url);
          })
          .find(function(item) {
            return Array.isArray(item);
          }),
        MITMrequest;

        // For non-GET requests, try the network first, fall back to the offline page
        if (request.method !== 'GET') {
            event.respondWith(
                fetch(request)
                    .catch(function () {
                        return caches.match(offline_page);
                    })
            );
            return;
        }

        // For HTML requests, try the network first, fall back to the cache, finally the offline page
        if (request.headers.get('Accept').indexOf('text/html') !== -1) {
            // Fix for Chrome bug: https://code.google.com/p/chromium/issues/detail?id=573937
            if (request.mode != 'navigate') {
                request = new Request(request.url, {
                    method: 'GET',
                    headers: request.headers,
                    mode: request.mode,
                    credentials: request.credentials,
                    redirect: request.redirect
                });
            }

            // if this is a RegExp match, strip anything not needed and fetch
            if(match) {
              MITMrequest = new Request(match[1], {
                method: 'GET',
                headers: request.headers,
                credentials: request.credentials,
                redirect: request.redirect
              });
            } else {
              MITMrequest = request.clone();
            }

            // otherwise, proceed with fetching the resource using the URI string literal
            event.respondWith(
                fetch(request)
                    .then(function (response) {
                        // NETWORK
                        // Stash a copy of this page in the pages cache
                        var copy = response.clone();
                        var cacheName = version + pagesCacheName;
                        stashInCache(cacheName, MITMrequest, copy);
                        return response;
                    })
                    .catch(function () {
                        // CACHE or FALLBACK
                        return caches.match(MITMrequest)
                            .then(function (response) {
                                return response || caches.match(offline_page);
                            })
                    })
            );
            return;
        }

        // For non-HTML requests, look in the cache first, fall back to the network
        event.respondWith(
            caches.match(request)
                .then(function (response) {
                    // CACHE
                    return response || fetch(request)
                        .then(function (response) {
                            // NETWORK
                            // If the request is for an image, stash a copy of this image in the images cache
                            if (request.headers.get('Accept').indexOf('image') !== -1) {
                                var copy = response.clone();
                                var cacheName = version + imagesCacheName;
                                stashInCache(cacheName, request, copy);
                            }
                            return response;
                        })
                        .catch(function () {
                            // OFFLINE
                            // If the request is for an image, show an offline placeholder
                            if (request.headers.get('Accept').indexOf('image') !== -1) {
                                return new Response('<svg role="img" aria-labelledby="offline-title" viewBox="0 0 400 300" xmlns="http://www.w3.org/2000/svg"><title id="offline-title">Offline</title><g fill="none" fill-rule="evenodd"><path fill="#D8D8D8" d="M0 0h400v300H0z"/><text fill="#9B9B9B" font-family="Helvetica Neue,Arial,Helvetica,sans-serif" font-size="72" font-weight="bold"><tspan x="93" y="172">offline</tspan></text></g></svg>', { headers: { 'Content-Type': 'image/svg+xml' }});
                            }
                        });
                })
        );
    });

})();
