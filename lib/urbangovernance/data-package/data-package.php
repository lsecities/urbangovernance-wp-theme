<?php
namespace UrbanGovernanceWPTheme;

class DataPackage {
  /**
   * @var String Identifier of the pod
   */
  const PODS_NAME = 'data_package';

  /**
   * @var String Identifier of the settings pod
   */
  const SETTINGS_PODS_NAME = 'data_settings';

  /**
   * @var Array The data package's data
   */
  protected $data_package;

  function __construct($query_type, $permalink) {
    /**
     * If a *specific* data package is being queried by id
     * (/api/v1/data-package/by-id/<id>), run the query by id
     */
    if($query_type === 'by-id') {
      $pod = pods(self::PODS_NAME, $permalink);
    }
    /**
     * Otherwise, check if a data package is being queried *by environment*
     * (`/api/v1/data-package/by-environment/<environment>`, where
     * `<environment>` can be `production` or `development`, or any other values
     * configured via Pods), and if so, check if one is defined (via
     * the Dataset Pods settings) and use that.
     */
    elseif($query_type === 'by-environment') {
      /**
       * Read settings
       */
      $settings_pod = pods(self::SETTINGS_PODS_NAME);
      $settings_pod->find();

      if($settings_pod->total_found()) {
        if($permalink === 'production') {
          $pod = pods(self::PODS_NAME, $settings_pod->field('production_dataset.permalink'));
        } elseif($permalink === 'development') {
          $pod = pods(self::PODS_NAME, $settings_pod->field('development_dataset.permalink'));
        }
      }
    }

    if(is_object($pod)) {
      $__data_package = [
        'world_map' => $pod->field('world_map._src.full'),
        'survey_results_data' => $pod->field('survey_results_data._src.full'),
        'titles_data' => $pod->field('titles_data._src.full'),
        'short_question_data' => $pod->field('short_question_data._src.full')
      ];

      $this->data_package = $__data_package;
    }
  }

  // Encode data_package configuration as JSON and return it as JSON string
  function to_json() {
    return json_encode($this->data_package);
  }
}
